package index;

import sas.controller.Controller;

/**
 * Classe che genera un programma di messaggistica criptata tra studenti
 * e consente di poter provare diversi tipi di tecniche di crittoanalisi
 */
public class MessageCrypt {

    public static void main(String[] args){
         Controller.getInstance();
    }
}
