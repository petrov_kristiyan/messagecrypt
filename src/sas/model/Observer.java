/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas.model;

/**
 *
 * @author kristiyan
 */
public interface Observer {
  public void update(Observable o,Object obj);
}
