package sas.model;

public class Mappatura {

    private final char[] mappatura;
    private final char[] mappaturaInversa;

    /**
     * Costruttore Mappatura calcola la mappatura inversa mappaturaInversa ha
     * come indice la posizione della lettera nel alfabeto, e come valore il
     * carattere corrispondente all'offset tra la sua posizione in mappatura e
     * la lettera a.<br> Es. a = 0 b = 1 c = 2 d = 3 e = 4 .... u = 20 ... z = 26
     * <br>mappatura : a,e,c,u <br>mappaturaInversa : a,*, c,*,b,e,...
     * 
     * <br>posizione alfabeto = indice array 
     * <br>a[ posizione 0 ] = a[ indice 0 ]
     * 
     * <br>e[posizione 4 ] = b[ indice 1 ( a + 1 = b ) ] 
     * <br>c[ posizione 2 ] = c[indice 2 ( a + 2 = c ) ] 
     * <br>u[ posizione 20 ] = e [ indice 3 ( a + 3 = e )]
     * @param c mappatura di caratteri
     */
    public Mappatura(char c[]) {
        mappatura = c.clone();
        mappaturaInversa = new char[26];
        for (int i = 0; i < c.length; i++) {
            mappaturaInversa[c[i] - 'a'] = (char) (i + 'a');
        }
    }

    /**
     * metodo map che resituisce il carattere in posizione c della mappatura
     * @param c indice del carattere da restituire
     * @return carattere in pos c
     */
    public char map(char c) {
        char res;
        if (Character.isAlphabetic(c)) {
            res = mappatura[(int) Character.toLowerCase(c) - 'a'];
        } else {
            res = c;
        }
        if (Character.isUpperCase(c)) {
            return Character.toUpperCase(res);
        } else {
            return res;
        }
    }
    /**
     * metodo inverseMap che restituisce il carettere in posizione c della mappaturaInversa
     * @param c indice del carattere da restituire
     * @return carattere in pos c della mappatura Inversa
     */
    public char inverseMap(char c) {
        char res;
        if (Character.isAlphabetic(c)) {
            res = mappaturaInversa[(int) Character.toLowerCase(c) - 'a'];
        } else {
            res = c;
        }
        if (Character.isUpperCase(c)) {
            return Character.toUpperCase(res);
        } else {
            return res;
        }
    }

    public char[] getMappatura() {
        return mappatura;
    }

    public char[] getMappaturaInversa() {
        return mappaturaInversa;
    }
    
}
