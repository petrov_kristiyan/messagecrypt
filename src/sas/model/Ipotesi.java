package sas.model;

import java.io.Serializable;

public class Ipotesi implements Serializable {

    private Character assegnamento;
    private boolean operazione; //undo false e assegnamento true
    private int timestamp;
    private String nome;
    private Character chiave;

    public Ipotesi(Character assegnamento) {
        this.assegnamento = assegnamento;
    }

    public Ipotesi(int timestamp, String nome, Character chiave, Character assegnamento, boolean operazione) {
        this.timestamp = timestamp;
        this.nome = nome;
        this.chiave = chiave;
        this.assegnamento = assegnamento;
        this.operazione = operazione;
    }

    public Ipotesi(Character assegnamento, boolean operazione, int timestamp, String nome) {
        this.assegnamento = assegnamento;
        this.operazione = operazione;
        this.timestamp = timestamp;
        this.nome = nome;
    }

    public Character getAssegnamento() {
        return assegnamento;
    }

    public void setAssegnamento(Character assegnamento) {
        this.assegnamento = assegnamento;
    }

    public boolean isOperazione() {
        return operazione;
    }

    public void setOperazione(boolean operazione) {
        this.operazione = operazione;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character getChiave() {
        return chiave;
    }

    public void setChiave(Character chiave) {
        this.chiave = chiave;
    }

    @Override
    public boolean equals(Object mossa) {
        boolean uguale = false;
        if (mossa != null && mossa instanceof Ipotesi) {
            uguale = assegnamento == ((Ipotesi) mossa).assegnamento;
        }
        return uguale;
    }

    public String toString() {
        String part1 = String.format("%-23s", timestamp);
        String part2 = String.format("%-23s", nome);
        String part3 = String.format("%-23s", chiave);
        return part1 + part2 + part3 + assegnamento;
    }
}
