/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas.model.frequenze;

import java.util.HashMap;
import java.util.Map;

public class FrequenzeDoppie extends Frequenza {

    @Override
    public Map<String, Double> frequenza(String lingua) {
        Map<String, Double> freq = new HashMap<>();
        if (lingua.equalsIgnoreCase("italiano")) {
            freq.put("LL", 0.94);
            freq.put("TT", 0.82);
            freq.put("LA", 1.25);
            freq.put("IA", 1.00);
            freq.put("ED", 0.79);
            freq.put("RE", 1.50);
            freq.put("CH", 1.04);
            freq.put("DI", 1.33);
            freq.put("RI", 1.14);
            freq.put("TI", 0.91);
            freq.put("SI", 0.81);
            freq.put("LI", 0.75);
            freq.put("AL", 1.41);
            freq.put("EL", 1.41);
            freq.put("IL", 0.72);
            freq.put("OL", 0.80);
            freq.put("ON", 1.76);
            freq.put("EN", 1.50);
            freq.put("AN", 1.42);
            freq.put("IN", 1.31);
            freq.put("CO", 1.41);
            freq.put("TN", 1.27);
            freq.put("NO", 1.20);
            freq.put("ER", 1.75);
            freq.put("AR", 1.18);
            freq.put("ES", 1.33);
            freq.put("OS", 0.96);
            freq.put("IS", 0.87);
            freq.put("AT", 1.12);
            freq.put("NT", 1.09);
            return freq;
        } else {
            freq.put("TH", 1.52);
            freq.put("HE", 1.28);
            freq.put("IN", 0.94);
            freq.put("ER", 0.94);
            freq.put("AN", 0.82);
            freq.put("RE", 0.68);
            freq.put("ND", 0.63);
            freq.put("AT", 0.59);
            freq.put("ON", 0.57);
            freq.put("NT", 0.56);
            freq.put("HA", 0.56);
            freq.put("ES", 0.56);
            freq.put("ST", 0.55);
            freq.put("EN", 0.55);
            freq.put("ED", 0.53);
            freq.put("TO", 0.52);
            freq.put("IT", 0.50);
            freq.put("OU", 0.50);
            freq.put("EA", 0.47);
            freq.put("HI", 0.46);
            freq.put("IS", 0.46);
            freq.put("OR", 0.43);
            freq.put("TI", 0.34);
            freq.put("AS", 0.33);
            freq.put("TE", 0.27);
            freq.put("ET", 0.19);
            return freq;
        }
    }

    @Override
    public Map<String, Double> frequenzaCalcolata(String testo) {
        Map<String, Double> freqContate = new HashMap<>();
        for (int i = 0; i < testo.length() - 1; i++) {
            char c1 = testo.charAt(i);
            char c2 = testo.charAt(i + 1);
            if (c1 != ' ' && c2 != ' ') {
                freqContate.put(c1 + "" + c2, (freqContate.get(c1 + "" + c2) == null ? 1 : freqContate.get(c1 + "" + c2) + 1.0));
            }
        }
        return freqContate;
    }
}
