package sas.model.frequenze;

import java.util.HashMap;
import java.util.Map;

public class FrequenzeSingole extends Frequenza {

    @Override
    public Map<String, Double> frequenza(String lingua) {
        Map<String, Double> freq = new HashMap<>();
        if (lingua.equalsIgnoreCase("italiano")) {
            freq.put("A", 11.74);
            freq.put("B", 0.92);
            freq.put("C", 4.50);
            freq.put("D", 3.73);
            freq.put("E", 11.79);
            freq.put("F", 1.15);
            freq.put("G", 1.64);
            freq.put("H", 0.63);
            freq.put("I", 10.14);
            freq.put("J", 0.01);
            freq.put("K", 0.009);
            freq.put("L", 6.51);
            freq.put("I", 2.51);
            freq.put("M", 6.88);
            freq.put("N", 9.83);
            freq.put("O", 3.05);
            freq.put("P", 0.50);
            freq.put("Q", 6.36);
            freq.put("R", 4.98);
            freq.put("S", 5.62);
            freq.put("T", 3.01);
            freq.put("U", 2.09);
            freq.put("V", 0.03);
            freq.put("W", 0.003);
            freq.put("X", 0.20);
            freq.put("Y", 1.18);
            freq.put("Z", 0.63);
            freq.put("à", 0.26);
            freq.put("è", 0.03);
            freq.put("ì", 0.002);
            freq.put("ù", 0.16);
            return freq;
        } else {
            freq.put("A", 8.16);
            freq.put("B", 1.49);
            freq.put("C", 2.78);
            freq.put("D", 4.25);
            freq.put("E", 12.70);
            freq.put("F", 2.22);
            freq.put("G", 2.01);
            freq.put("H", 6.09);
            freq.put("I", 6.96);
            freq.put("J", 0.15);
            freq.put("K", 0.77);
            freq.put("I", 4.02);
            freq.put("M", 2.40);
            freq.put("N", 6.74);
            freq.put("O", 7.50);
            freq.put("P", 1.92);
            freq.put("Q", 0.09);
            freq.put("R", 5.98);
            freq.put("S", 6.32);
            freq.put("T", 9.05);
            freq.put("U", 2.75);
            freq.put("V", 0.97);
            freq.put("W", 2.36);
            freq.put("X", 0.15);
            freq.put("Y", 1.97);
            freq.put("Z", 0.07);
            return freq;
        }
    }

    @Override
    public Map<String, Double> frequenzaCalcolata(String testo) {
        Map<String, Double> freqContate = new HashMap<>();
        for (int i = 0; i < testo.length(); i++) {
            Character c = testo.charAt(i);
            double count = 0;
            if (c != ' ') {
                for (int k = 0; k < testo.length(); k++) {
                    if (c == testo.charAt(k)) {
                        count++;
                    }
                }
                freqContate.put(c + "", count);
            }
        }
        return freqContate;
    }
}
