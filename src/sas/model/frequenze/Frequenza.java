package sas.model.frequenze;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class Frequenza {

    public static Frequenza richiediFrequenza(String tipo) {
        if (tipo.equalsIgnoreCase("singola")) {
            FrequenzeSingole singole = new FrequenzeSingole();
            return singole;
        } else {
            FrequenzeDoppie doppie = new FrequenzeDoppie();
            return doppie;
        }
    }

    public abstract Map<String, Double> frequenza(String lingua);

    public static Map<String, Double> trimFreq(Map<String, Double> freq) {
        Map<String, Double> newMap = new LinkedHashMap<>();
        int count = 0;
        for (Map.Entry<String, Double> entry : freq.entrySet()) {
            if (count < 30) {
                newMap.put(entry.getKey(), entry.getValue());
            } else {
                break;
            }
            count++;
        }
        return newMap;
    }
    public abstract Map<String,Double> frequenzaCalcolata(String testo);
}
