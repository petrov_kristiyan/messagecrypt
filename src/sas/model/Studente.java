package sas.model;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class Studente implements Spia{

    private String nome;
    private String cognome;
    private int id;
    private String username;
    private String pwd;

    public Studente(String nome, String cognome, int id, String username, String pwd) {
        this.nome = nome;
        this.cognome = cognome;
        this.id = id;
        this.username = username;
        this.pwd = pwd;
    }

    public Studente(CachedRowSet info) throws SQLException {
        while (info.next()) {
            this.id = info.getInt("ID");
            this.nome = info.getString("NOME");
            this.cognome = info.getString("COGNOME");
            this.username = info.getString("USERNAME");
            this.pwd = info.getString("PASSWORD");
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
