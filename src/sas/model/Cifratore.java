package sas.model;

import java.text.Normalizer;

/**
 * Classe per cifrare e decifrare tramite una mappatura un testo
 *
 */
public class Cifratore{
    /**
     * Metodo utilizzato per cifrare il testo con la mappatura passata come parametro
     * 
     * @param map Mappatura da utilizzare richiama il metodo map per ogni lettera del testo
     * @param testo testo da cifrare
     * @return  testo cifrato
     */
    public static String cifra(Mappatura map, String testo) {
        testo = Normalizer.normalize(testo, Normalizer.Form.NFD);
        String testoCifrato="";
        for(int i=0;i<testo.length();i++){
           testoCifrato += map.map(testo.charAt(i));
        }
        return testoCifrato;
    }
    /**
     * Metodo utiilzzato per decifrare il testo con la mappatura passata come parametro
     * 
     * @param map Mappatura da utilizzare richiame il metodo inverseMap per ogni lettera del testo
     * @param testo testo da decifrare
     * @return testo decifrato
     */
    public static String decifra(Mappatura map, String testo) {
        String testoDecifrato = "";
        for(int i=0;i<testo.length();i++){
           testoDecifrato += map.inverseMap(testo.charAt(i));
        }
        return testoDecifrato;
    }

}
