package sas.model;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;
import sas.controller.DBManager;

public class Proposta {

    private int id; // id della proposta
    private String stato; //stato  denied, accepted,  pending, expired 
    private boolean notificata; // cioè se è stata vista 
    private UserInfo proponente; // altrimenti detto user  colui che propone
    private UserInfo partner; // a chi
    private SistemaDiCifratura sdc; //sistema di cifratura

    public Proposta(CachedRowSet info) throws SQLException, Exception {
        id = info.getInt("ID");
        stato = info.getString("STATO");
        notificata = info.getInt("NOTIFICATA")!=0; //true se notificata ==1 false altrimenti
        proponente = new UserInfo(DBManager.executeQuery("SELECT * FROM STUDENTE WHERE ID=" + info.getInt("CREATORE")));
        partner = new UserInfo(DBManager.executeQuery("SELECT * FROM STUDENTE WHERE ID=" + info.getInt("PARTNER")));
        this.sdc = SistemaDiCifratura.load(info.getInt("idSdc"));
    }

    public Proposta(UserInfo prop, UserInfo part, SistemaDiCifratura sdc) throws SQLException {
        stato = "pendente";
        proponente = prop;
        partner = part;
        notificata = false;
        this.sdc = sdc;
    }
    /**
     * Metodo che restituisce la proposta attualmente in uso
     * 
     * @param prop Creatore della proposta
     * @param part Partner 
     * @return Proposta
     * @throws SQLException errore lato DB
     * @throws Exception errore generale
     */
    public static Proposta caricaAttiva(UserInfo prop, UserInfo part) throws SQLException, Exception {
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM PROPOSTA WHERE (CREATORE=" + prop.getId() + " AND PARTNER =" + part.getId() + " )OR ( CREATORE ="+part.getId()+" AND PARTNER ="+prop.getId()+" ) and stato='accettata'");
        Proposta p = null;
        while (rs.next()) {
            p = new Proposta(rs);
        }
        return p;
    }

    /**
     * Metodo salva , serve per inserire una proposta nel DB
     *
     * @return true se l'operazione è avvenuta con successo
     * @throws SQLException errore lato DB
     */
    public boolean salva() throws SQLException {
        String s = "INSERT INTO PROPOSTA (STATO,NOTIFICATA,CREATORE,PARTNER,IDSDC) VALUES('" + stato + "'," + getNotificata() + "," + proponente.getId() + "," + partner.getId() + "," + sdc.getId() + ")";
        return DBManager.executeUpdate(s) != 0;
    }

    /**
     * Aggiorna Proposta esistente
     *
     * @return true se l'operazione ha avuto successo
     * @throws SQLException errore lato DB
     */
    public boolean aggiorna() throws SQLException {
        String s = "UPDATE PROPOSTA set STATO='" + stato + "',NOTIFICATA=" + getNotificata() + ",CREATORE=" + proponente.getId() + ",PARTNER=" + partner.getId() + ",IDSDC=" + sdc.getId() + " where ID=" + id + "";
        return DBManager.executeUpdate(s) != 0;
    }

    /**
     * Imposta lo stato della Proposta
     * @param stato accettata,rifiutata,scaduta
     */
    public void setStato(String stato) {
        this.stato = stato;
    }
    /**
     * Metodo per sapere se la proposta è stata notificata
     * 
     * @return 1 se è stata notificata/visualizzata 0, altrimenti
     */
    public int getNotificata() {
         if(notificata)
             return 1;
         else
             return 0;
    }
    /**
     * Imposta lo stato di notifica della proposta
     * 
     * @param notificata true o false
     */
    public void setNotificata(boolean notificata) {
        this.notificata = notificata;
    }
    /**
     * Metodo per sapere il creatore della proposta
     * 
     * @return UserInfo
     */
    public UserInfo getProponente() {
        return proponente;
    }
    /**
     * Metodo per sapere il partner dell'utente creatore della proposta
     * 
     * @return  UserInfo
     */
    public UserInfo getPartner() {
        return partner;
    }
    
    /**
     * Restituisce il sistema di cifratura della proposta
     * 
     * @return sistema di cifratura
     */
    public SistemaDiCifratura getSdc() {
        return sdc;
    }
    /**
     * Impostazione del sistema di cifratura
     * 
     * @param sdc sistema di cifratura da utilizzare
     */
    public void setSdc(SistemaDiCifratura sdc) {
        this.sdc = sdc;
    }
    /**
     * Metodo di Stampa formattata del username, chiave, metodo e stato della Proposta
     * 
     * @return stringa formattata
     */
    public String toString() {
        String part1 = String.format("%-20s", partner.getUsername());
        String part2 = String.format("%-20s", proponente.getUsername());
        String part3 = String.format("%-20s", sdc.getChiave());
        String part4 = String.format("%-20s", sdc.getMetodo());
        return part2 +part1+ part3 + part4 + stato;
    }
}
