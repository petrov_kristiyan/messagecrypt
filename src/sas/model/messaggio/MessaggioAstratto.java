package sas.model.messaggio;

import java.sql.SQLException;
import sas.model.UserInfo;

public interface MessaggioAstratto {
    public String getTesto();
    public String getTestoCifrato();
    public String getLingua();
    public String getTitolo();
    public UserInfo getDestinatario();
    public UserInfo getMittente();
    public int getId();
    public boolean elimina() throws SQLException;
}
