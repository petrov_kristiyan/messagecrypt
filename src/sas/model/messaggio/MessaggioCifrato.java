package sas.model.messaggio;

public interface MessaggioCifrato {
    
    public void setTestoCifrato(String testoCifrato);

    public String getTestoCifrato();

}
