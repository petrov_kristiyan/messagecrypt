package sas.model.messaggio;

public interface MessaggioDestinatario extends MessaggioAstratto{
    public int getLetto();
    public void decifra();
}
