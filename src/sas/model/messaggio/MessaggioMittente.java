package sas.model.messaggio;

import java.sql.SQLException;

public interface MessaggioMittente extends MessaggioAstratto{
    public  int getBozza();
    public  boolean salva() throws SQLException;
    public  void cifra() throws SQLException,Exception;   

    public void setId(int id);
}
