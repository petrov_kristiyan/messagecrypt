package sas.model.messaggio;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import sas.controller.DBManager;
import sas.model.Cifratore;
import sas.model.Mappatura;
import sas.model.Proposta;
import sas.model.SistemaDiCifratura;
import sas.model.Studente;
import sas.model.UserInfo;

public class Messaggio implements MessaggioDestinatario, MessaggioMittente ,MessaggioCifrato{

    private int id;
    private String testo;
    private String testoCifrato;
    private String lingua;
    private String titolo;
    private boolean bozza;
    private boolean letto;
    private UserInfo mittente;
    private UserInfo destinatario;
    private SistemaDiCifratura sdc;

    public Messaggio(CachedRowSet rs) throws SQLException, Exception {
        id = rs.getInt("ID");
        testo = rs.getString("TESTO");
        testoCifrato = rs.getString("TESTOCIFRATO");
        lingua = rs.getString("LINGUA");
        titolo = rs.getString("TITOLO");
        bozza = rs.getInt("BOZZA") != 0;
        letto = rs.getInt("LETTO") != 0;
        destinatario = new UserInfo(DBManager.executeQuery("SELECT * FROM STUDENTE WHERE ID=" + rs.getInt("IDDESTINATARIO")));
        mittente = new UserInfo(DBManager.executeQuery("SELECT * FROM STUDENTE WHERE ID=" + rs.getInt("IDMITTENTE")));
        sdc = SistemaDiCifratura.load(rs.getInt("SDC"));
    }

    public Messaggio(String testo, String lingua, String titolo, boolean bozza, boolean letto, UserInfo mittente, UserInfo destinatario) {
        this.testo = testo;
        this.lingua = lingua;
        this.titolo = titolo;
        this.bozza = bozza;
        this.letto = letto;
        this.mittente = mittente;
        this.destinatario = destinatario;
    }
    
    public Messaggio(String testoC){
        this.testoCifrato = testoC;
    }

    /**
     * Metodo load per ottenere un messaggio dal DB tramite il suo id
     *
     * @param id identificativo del messaggio da caricare
     * @return nuovo Messaggio
     * @throws SQLException errore lato DB
     */
    public static Messaggio load(int id) throws SQLException, Exception {
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM MESSAGGIO WHERE ID=" + id + "");
        Messaggio mex = null;
        while (rs.next()) {
            mex = new Messaggio(rs);
        }
        return mex;
    }

    /**
     * Metodo caricaBozze serve per ottenere la lista delle bozze dello studente
     *
     * @param stud Studente del quale caricare le Bozze
     * @return Lista di Messaggi
     * @throws SQLException errore lato DB
     */
    public static List<MessaggioMittente> caricaBozze(Studente stud) throws SQLException, Exception {
        List<MessaggioMittente> mes = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM MESSAGGIO WHERE BOZZA=1" + "AND IDMITTENTE=" + stud.getId() + "");
        while (rs.next()) {
            mes.add(new Messaggio(rs));
        }
        return mes;
    }

    /**
     * Metodo caricaInviati serve per ottenere i messaggi inviati dallo Studente
     *
     * @param stud Studente del quale si vogliono ottenere i Messaggi inviati
     * @return Lista dei Messaggi Inviati
     * @throws SQLException errore lato DB
     */
    public static List<MessaggioMittente> caricaInviati(Studente stud) throws SQLException, Exception {
        List<MessaggioMittente> mes = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM MESSAGGIO WHERE BOZZA=0 AND IDMITTENTE=" + stud.getId() + "");
        while (rs.next()) {
            mes.add(new Messaggio(rs));
        }

        return mes;
    }

    /**
     * Metodo caricaRicevuti serve per ottenere i messaggi ricevuti dallo
     * Studente
     *
     * @param stud Studente del quale si vogliono ottenere i Messaggi ricevuti
     * @return Lista dei Messaggi Ricevuti
     * @throws SQLException errore lato DB
     */
    public static List<MessaggioDestinatario> caricaRicevuti(Studente stud) throws SQLException, Exception {
        List<MessaggioDestinatario> mes = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM MESSAGGIO WHERE BOZZA=0 AND IDDESTINATARIO=" + stud.getId() + "");
        while (rs.next()) {
            mes.add(new Messaggio(rs));
        }
        return mes;
    }

    /**
     * Metodo elimina serve per eliminare un Messaggio dal DB
     *
     * @return true se l'operazione è stata eseguita con successo false
     * altrimenti
     * @throws SQLException errore lato DB
     */
    public boolean elimina() throws SQLException {
        String s = "DELETE FROM MESSAGGIO WHERE ID=" + getId() + "";
        return DBManager.executeUpdate(s) != 0;
    }

    /**
     * metodo salva serve per inserire o aggiornare un Messaggio nel DB
     *
     * @return true se l'operazione è stata eseguita con successo false
     * altrimenti
     * @throws SQLException errore lato DB
     */
    public boolean salva() throws SQLException {
        CachedRowSet rs = DBManager.executeQuery("SELECT COUNT(*) as COUNT FROM MESSAGGIO WHERE ID =" + getId() + "");
        while (rs.next()) {
            if (rs.getInt("COUNT") == 0) {
                String s = "";
                if (bozza && destinatario == null) {
                    s = "INSERT INTO MESSAGGIO (TESTO,TESTOCIFRATO,LINGUA,TITOLO,BOZZA,LETTO,IDDESTINATARIO,IDMITTENTE,SDC) VALUES( '" + testo + "', '" + testoCifrato + "','" + lingua + "','" + titolo + "'," + getBozza() + "," + getLetto() + "," + destinatario + "," + mittente.getId() + "," + sdc.getId() + ")";
                } else {
                    s = "INSERT INTO MESSAGGIO (TESTO,TESTOCIFRATO,LINGUA,TITOLO,BOZZA,LETTO,IDDESTINATARIO,IDMITTENTE,SDC) VALUES( '" + testo + "', '" + testoCifrato + "','" + lingua + "','" + titolo + "'," + getBozza() + "," + getLetto() + "," + destinatario.getId() + "," + mittente.getId() + "," + sdc.getId() + ")";
                }
                return DBManager.executeUpdate(s) != 0;
            } else {
                System.out.println(sdc.getId());
                String s = "UPDATE MESSAGGIO set TESTO='" + testo + "',TESTOCIFRATO='" + testoCifrato + "',LINGUA='" + lingua + "',TITOLO='" + titolo + "',BOZZA=" + getBozza() + ",LETTO=" + getLetto() + ",IDDESTINATARIO=" + destinatario.getId() + ",SDC=" + sdc.getId() + ",IDMITTENTE=" + mittente.getId() + " where ID=" + id;
                return DBManager.executeUpdate(s) != 0;
            }
        }
        return false;
    }

    /**
     * Metodo che cifra il messaggio con il sistema di cifratura proposto
     * accettato
     *
     * @throws SQLException errore lato DB
     */
    public void cifra() throws SQLException, Exception {
        Proposta p = Proposta.caricaAttiva(mittente, destinatario);
        this.sdc = p.getSdc();
        this.testoCifrato = Cifratore.cifra(sdc.getMappatura(), this.testo);
    }

    /**
     * Metodo che decifra il messaggio con il sistema di cifratura
     */
    @Override
    public void decifra() {
        Mappatura map = this.sdc.getMappatura();
        this.testo = Cifratore.decifra(map, testoCifrato);
    }

    /**
     * Metodo per inviare il messaggio
     *
     * @return true se l'operazione è avvenuta con successo
     * @throws SQLException errore lato DB
     */
    public boolean send() throws SQLException {
        return salva();
    }

    public UserInfo getMittente() {
        return mittente;
    }

    public void setIdMittente(UserInfo mittente) {
        this.mittente = mittente;
    }

    @Override
    public UserInfo getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(UserInfo destinatario) {
        this.destinatario = destinatario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    @Override
    public String getTestoCifrato() {
        return testoCifrato;
    }

    public void setTestoCifrato(String testoCifrato) {
        this.testoCifrato = testoCifrato;
    }

    @Override
    public String getLingua() {
        return lingua;
    }

    public void setLingua(String lingua) {
        this.lingua = lingua;
    }

    @Override
    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public int getBozza() {
        return bozza ? 1 : 0;
    }

    public void setBozza(boolean bozza) {
        this.bozza = bozza;
    }

    @Override
    public int getLetto() {
        return letto ? 1 : 0;
    }

    public void setLetto(boolean letto) {
        this.letto = letto;
    }

    public SistemaDiCifratura getSdc() {
        return sdc;
    }

    public void setSdc(SistemaDiCifratura sdc) {
        this.sdc = sdc;
    }
    /**
     * Stampa username del mittente e il titolo del messaggio
     * @return stringa formattata 
     */
    public String toString() {
        String part1 = String.format("%-47s", getMittente().getUsername());
        return part1 + getTitolo();
    }
}
