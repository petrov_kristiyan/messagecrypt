package sas.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import sas.controller.DBManager;
import sas.model.calcolatore.CalcolatoreMappe;
/**
 * Classe Sistema di Cifratura 
 */
public class SistemaDiCifratura {

    private int id;
    private final String chiave;
    private final String metodo;
    private Mappatura mappatura;
    /**
     * 
     * @param chiave chiave di cifratura a seconda del metodo può essere di diverso tipo
     * @param metodo metodo di cifratura ( Cesare, Pseudocasuale , Chiave )
     * @throws SQLException errore lato DB
     * @throws Exception errore generando la mappatura
     */
    public SistemaDiCifratura(String chiave, String metodo) throws SQLException, Exception  {
        CalcolatoreMappe cm = CalcolatoreMappe.create(metodo);
        this.mappatura = cm.calcola(chiave);
        this.metodo = metodo;
        this.chiave = chiave;
    }
    /**
     * 
     * @param rs query passata per creare il sistema di cifratura
     * @throws SQLException errore lato DB
     * @throws Exception errore in genera Mappatura
     */
    public SistemaDiCifratura(CachedRowSet rs) throws SQLException, Exception {
        this.id = rs.getInt("ID");
        this.chiave = rs.getString("CHIAVE");
        this.metodo = rs.getString("METODO");
        CalcolatoreMappe cm = CalcolatoreMappe.create(metodo);
        this.mappatura = cm.calcola(chiave);
    }

    /**
     * Carica i sistemi di cifratura utilizzati dallo studente stud
     *
     * @param stud creatore
     * @return lista dei sistemi di cifratura
     * @throws SQLException errore lato DB
     */
    public static List<SistemaDiCifratura> caricaSistemaDiCifratura(Studente stud) throws SQLException, Exception {
        List<SistemaDiCifratura> sdc = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM SISTEMADICIFRATURA where CREATORE=" + stud.getId());
        while (rs.next()) {
            sdc.add(new SistemaDiCifratura(rs));
        }
        return sdc;
    }

    /**
     * Carica dal DB il sistema di cifratura con l'id richiesto
     *
     * @param id id del sistema di cifratura
     * @return sistema di cifratura richiesto
     * @throws SQLException errore lato DB
     */
    public static SistemaDiCifratura load(int id) throws SQLException, Exception {
        SistemaDiCifratura sdc = null;
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM SISTEMADICIFRATURA WHERE id = " + id);
        if (rs.next()) {
            sdc = new SistemaDiCifratura(rs);
        }
        return sdc;
    }

    /**
     * Prova a cifrare il testo tramite il Cifratore e la mappatura del sistema di cifratura
     * 
     * @param testo testo da cifrare
     * @return testo cifrato 
     */
    public String prova(String testo) {
        return Cifratore.cifra(mappatura, testo);
    }

    /**
     * Inserisce o aggiorna nel DB il sistema di cifratura
     *
     * @param user creatore del sistema di cifratura
     * @return true se l operazione è stata effettuata con successo, false
     * altrimenti
     * @throws SQLException errore lato DB
     */
    public boolean salva(Studente user) throws SQLException {
        String s = "INSERT INTO SISTEMADICIFRATURA (CHIAVE,METODO,CREATORE) VALUES(?,?,?)";
        PreparedStatement ps = DBManager.createStatement(s);
        int affectedRows = 1;
        try {
            ps.setString(1, getChiave());
            ps.setString(2, getMetodo());
            ps.setInt(3, user.getId());
            affectedRows = ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("problema ID");
                }
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return affectedRows != 0;
    }

    /**
     * Elimina sitema di cifratura dal DB
     *
     * @return true se l operazione è stata effettuata con successo, false
     * altrimenti
     * @throws SQLException errore lato DB
     */
    public boolean elimina() throws SQLException {
        String s = "DELETE FROM SISTEMADICIFRATURA WHERE ID=" + id + "";
        return DBManager.executeUpdate(s) != 0;
    }

    /**
     * Metodo per cambiare metodo di cifratura o chiave senza dover ricreare il
     * metodo di cifratura
     * 
     * @throws java.lang.Exception errore della chiave in input
     */
    public void calcolaMappatura() throws Exception {
        CalcolatoreMappe cm = CalcolatoreMappe.create(this.metodo);
        this.mappatura = cm.calcola(this.chiave);
    }
    /**
     * Metodo che restituisce l'id del sistema di cifratura
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * Ritorna la chaive del sistema di cifratura
     * 
     * @return chiave scelta dal creatore del sistema di cifratura
     */
    public String getChiave() {
        return chiave;
    }
    /**
     * Ritorna il metodo di cifratura del sistema
     * @return metodo di tipo(Cesare,Pseudocasuale,Chiave)
     */
    public String getMetodo() {
        return metodo;
    }
    /**
     * Metodo che ritorna la mappatura generata per il sistema di cifratura
     * @return mapaptura
     */
    public Mappatura getMappatura() {
        return mappatura;
    }

    /**
     * Stamap formattata
     * 
     * @return  chiave e metodo del sistema di Cifratura
     */
    public String toString() {
        String part1 = String.format("%-47s", getChiave());
        return part1 + getMetodo();
    }
}
