package sas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class AlberoIpotesi implements Serializable {

    private LinkedHashMap<Character, List<Ipotesi>> albero;
    private static final long serialVersionUID = 1L;

    public AlberoIpotesi(LinkedHashMap<Character, List<Ipotesi>> albero) {
        this.albero = albero;
    }

    public LinkedHashMap<Character, List<Ipotesi>> getAlbero() {
        return albero;
    }

    public void setAlbero(LinkedHashMap<Character, List<Ipotesi>> albero) {
        this.albero = albero;
    }

    public List<Ipotesi> getMosseOrdinate() {
        List<Ipotesi> lista = new ArrayList<>();
        albero.forEach((k, v) -> {
            List<Ipotesi> listaM = albero.get(k);
            listaM.forEach(e -> lista.add(new Ipotesi(e.getTimestamp(), e.getNome(),k, e.getAssegnamento(), e.isOperazione())));
        });
        Collections.sort(lista, (Ipotesi o1, Ipotesi o2) -> Integer.compare(o2.getTimestamp(), o1.getTimestamp()));
        return lista;
    }
    
    public int getMaxTimestamp(){
       List<Ipotesi> lista = getMosseOrdinate();
        if (lista.size() > 0) {
           return lista.get(lista.size() - 1).getTimestamp();
        }
        return 0;
    }
    public int firstIndexOf(List<Ipotesi> list, Ipotesi searchedObject) {
        int i = 0;
        for (Ipotesi o : list) {
            if (Objects.equals(o.getAssegnamento(), searchedObject.getAssegnamento())&& Objects.equals(o.getChiave(), searchedObject.getChiave())) {
               return i;
            }
            i++;
        }
        return -1;
    }
    public int lastIndexOf(List<Ipotesi> list, Ipotesi searchedObject) {
        int i = 0,last=-1;
        for (Ipotesi o : list) {
            if (Objects.equals(o.getAssegnamento(), searchedObject.getAssegnamento())) {
                last=i;
            }
            i++;
        }
        return last;
    }
    public boolean undoIpotesi(Character chiave, Ipotesi ipotesi){
       return albero.get(chiave).add(ipotesi);
    }
    public void aggiungiIpotesi(Character chiave,List<Ipotesi> listaIpotesi, Ipotesi ipotesi){
        listaIpotesi.add(ipotesi);
        albero.put(chiave, listaIpotesi);
    }
}
