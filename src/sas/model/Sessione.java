package sas.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Observable;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.serial.SerialBlob;
import sas.controller.DBManager;
import sas.model.messaggio.Messaggio;
import sas.model.messaggio.MessaggioCifrato;

public class Sessione extends Observable implements Serializable {

    private MessaggioCifrato messaggioCifrato;
    private String nome;
    private AlberoIpotesi albero;
    private static  Spia spia ;
    private int id;
    private Soluzione soluzione;

    public Sessione(Spia user) throws SQLException, Exception {
        spia = user;
        LinkedHashMap<Character,List<Ipotesi>> lhm = new LinkedHashMap<>();
        albero = new AlberoIpotesi(lhm);
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM MESSAGGIO WHERE BOZZA= 0 ORDER BY RANDOM() OFFSET 0 ROWS FETCH NEXT 1 ROW ONLY");
        while (rs.next()) {
            this.messaggioCifrato = (MessaggioCifrato) new Messaggio(rs);
        }
        String testoCifrato ="";
        if(messaggioCifrato!=null){
         testoCifrato = messaggioCifrato.getTestoCifrato();
        }
        soluzione = new Soluzione();
        soluzione.setTestoIpotizzato(testoCifrato);
        id=0;
    }

    public Sessione(CachedRowSet rs) throws SQLException, IOException, ClassNotFoundException {
        id = rs.getInt("ID");
        nome = rs.getString("NOME");
        String testoCifrato = rs.getString("TESTOCIFRATO");
        messaggioCifrato = (MessaggioCifrato) new Messaggio(testoCifrato); 
        Blob blob = rs.getBlob("ALBEROIPOTESI");
        ByteArrayInputStream bais = new ByteArrayInputStream(blob.getBytes(1, (int) blob.length()));
        ObjectInputStream ois = new ObjectInputStream(bais);
        albero = (AlberoIpotesi) ois.readObject();
        String testoIpotizzato =rs.getString("TESTOIPOTETIZZATO"); 
        soluzione = new Soluzione();
        soluzione.setTestoIpotizzato(testoIpotizzato);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome){
        this.nome=nome;
    }
    public static List<Sessione> elencaSessioni(Spia user) throws SQLException, IOException, ClassNotFoundException {
        List<Sessione> listaSessioni = new ArrayList();
        spia = user;
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM SESSIONE WHERE IDUSER=" + user.getId());
        while (rs.next()) {
            listaSessioni.add(new Sessione(rs));
        }
        return listaSessioni;
    }

    public Sessione load(int id) throws SQLException, IOException, ClassNotFoundException {
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM SESSIONE WHERE ID=" + id);
        if (rs.next()) {
            return new Sessione(rs);
        }
        return null;
    }

    public boolean salva() throws SQLException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oout;
        oout = new ObjectOutputStream(baos);
        oout.writeObject(albero);
        oout.flush();
        Blob blob = new SerialBlob(baos.toByteArray());
        String s = "INSERT INTO SESSIONE(NOME,TESTOCIFRATO,TESTOIPOTETIZZATO,IDUSER,ALBEROIPOTESI) VALUES(?,?,?,?,?)";
        PreparedStatement ps = DBManager.createStatement(s);
        int affectedRows = 1;
        try {
            ps.setString(1, nome);
            ps.setString(2, messaggioCifrato.getTestoCifrato());
            ps.setString(3, soluzione.getTestoIpotizzato());
            ps.setInt(4, spia.getId());
            ps.setBlob(5, blob);
            affectedRows = ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("problema ID");
                }
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return affectedRows != 0;
    }

    public boolean eliminaSessione() throws SQLException {
        String s = "DELETE FROM SESSIONE WHERE ID=" + id + "";
        return DBManager.executeUpdate(s) != 0;
    }

    public boolean aggiornaSessione() throws IOException, SQLException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oout;
        Blob blob;
        oout = new ObjectOutputStream(baos);
        oout.writeObject(albero);
        oout.flush();
        blob = new SerialBlob(baos.toByteArray());
        String s = "UPDATE SESSIONE SET NOME=?,TESTOCIFRATO=?,TESTOIPOTETIZZATO=?,IDUSER=?,ALBEROIPOTESI=? WHERE ID=" + id;
        PreparedStatement ps = DBManager.createStatement(s);
        ps.setString(1, nome);
        ps.setString(2, messaggioCifrato.getTestoCifrato());
        ps.setString(3, soluzione.getTestoIpotizzato());
        ps.setInt(4, spia.getId());
        ps.setBlob(5, blob);
        return ps.executeUpdate() != 0;
    }

    public AlberoIpotesi getAlbero() {
        return albero;
    }

    public void setAlbero(AlberoIpotesi albero) {
        this.albero = albero;
    }

    public MessaggioCifrato getMessaggioCifrato() {
        return messaggioCifrato;
    }

    public void setMessaggioCifrato(MessaggioCifrato messaggioCifrato) {
        this.messaggioCifrato = messaggioCifrato;
    }

    public String toString() {
        return nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public Soluzione getSoluzione(){
        return soluzione;
    }
    
}
