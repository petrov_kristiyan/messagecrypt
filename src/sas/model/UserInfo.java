package sas.model;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;

public class UserInfo {

    private int id;
    private String nome;
    private String cognome;
    private String username;

    public UserInfo(int id, String nome, String cognome, String username) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.username = username;
    }
    
    public UserInfo(CachedRowSet info) throws SQLException {
        while (info.next()) {
            this.id = info.getInt("ID");
            this.nome = info.getString("Nome");
            this.cognome = info.getString("COGNOME");
            this.username = info.getString("USERNAME");
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    /**
     * Stampa l'username del utente
     * @return username
     */
    public String toString() {
        return getUsername();
    }

}
