package sas.model.calcolatore;

import sas.model.Mappatura;

public abstract class CalcolatoreMappe {

    /**
     *Metodo che crea la mappatura a seconda del Metodo richiesto
     * 
     * @param met metodo di cifratura scelto Cesare,Pseudocasuale,Chiave
     * @return Calcolatore adeguato
     */
    public static CalcolatoreMappe create(String met){
        if (met.equals("Chiave")) {
            CalcolatoreChiave cm = new CalcolatoreChiave();
            return cm;
        } else {
            if (met.equals("Pseudocasuale")) {
                CalcolatorePseudo cm = new CalcolatorePseudo();
                return cm;
            } else {
                CalcolatoreCesare cm = new CalcolatoreCesare();
                return cm;
            }
        }
    } 
    public abstract Mappatura calcola(String chiave) throws Exception;
}
