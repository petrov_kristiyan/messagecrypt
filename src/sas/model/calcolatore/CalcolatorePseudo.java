package sas.model.calcolatore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import sas.model.Mappatura;
/**
 * Calcolatore Pseudo che implementa shuffle del alfabeto  con seed la chiave 
 * se la chiave è una stringa il seed è equivalente al numero composto dalla differenza tra il carattere e 'a'
 */
public class CalcolatorePseudo extends CalcolatoreMappe {

    private final List<Character> alfabeto;

    public CalcolatorePseudo() {
        Character[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        alfabeto = Arrays.asList(chars);
    }
    /**
     * Metodo che calcola la mappatura per la cifratura Pseudocasuale
     * 
     * @param chiave di tipo alfanumerica
     * @return mappatura generata
     */
    public Mappatura calcola(String chiave) {
        char mappa[] = new char[26];
        String numero = "";
        for (int i = 0; i < chiave.length(); i++) {
            if (!Character.isDigit(chiave.charAt(i))) {
                numero = numero+""+(chiave.charAt(i)-'a');
            } else {
                numero = numero+"" + chiave.charAt(i);
            }
        }
        Collections.shuffle(alfabeto, new Random(Integer.parseInt(numero)));
        Character[] tmp = new Character[26];
        alfabeto.toArray(tmp);
        int i = 0;
        for (Character c : tmp) {
            mappa[i] = c;
            i++;
        }
        return new Mappatura(mappa);
    }
}
