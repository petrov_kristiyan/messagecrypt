package sas.model.calcolatore;

import sas.model.Mappatura;

public class CalcolatoreCesare extends CalcolatoreMappe {

    /**
     * Metodo che calcola la mappatura per la cifratura di Cesare
     *
     * @param chiave numero intero
     * @return Mappatura generata
     * @throws Exception errore tipo chiave
     */
    public Mappatura calcola(String chiave) throws Exception {
        String result = "";
        for (int i = 0; i < chiave.length(); i++) {
            if (!Character.isDigit(chiave.charAt(i))) {
                throw new Exception("Errore : la chiave inserita per fare la codifica di cesare non è composta solo da numeri");
            }
        }
        int offset = Integer.parseInt(chiave);
        offset = offset % 26;
        //shifta alfabeto con l offset
        for (int i = offset + 'a'; result.length() < 26; i++) {
            if (i > 'z') {
                i = 'a';
            }
            result += "" + (char) i;
        }
        Mappatura m = new Mappatura(result.toCharArray());
        return m;
    }

}
