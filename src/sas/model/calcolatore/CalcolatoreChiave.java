package sas.model.calcolatore;

import sas.model.Mappatura;

public class CalcolatoreChiave extends CalcolatoreMappe {
    /**
     * Metodo che calcola la mappatura per la cifratura con Chiave
     * @param chiave parola utilizzata come chiave
     * @return Mappatura generata
     * @throws Exception errore tipo chiave 
     */
    public Mappatura calcola(String chiave)  throws Exception{
        String result = "";
        //rimozione lettere duplicate nella chiave
        for (int i = 0; i < chiave.length(); i++) {
            if (!Character.isLetter(chiave.charAt(i))) {
                throw new Exception("Errore: la chiave inserita contiene caratteri non appartenenti all' alfabeto");
            }
            if (!result.contains("" + chiave.charAt(i))) {
                result += "" + chiave.charAt(i);
            }
        }
        char c = result.charAt(result.length() - 1);
        //append resto del alfabeto alla chiave in ordine alfabetico
        for (int i = c; result.length() < 26; i++) {
            if (i > 'z') {
                i = 'a';
            }
            if (!result.contains("" + (char) i)) {
                result += "" + (char) i;
            }
        }
        Mappatura m = new Mappatura(result.toCharArray());
        return m;
    }
}
