package sas.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public final class PlaceholderTextField extends JTextField {

    private Font originalFont;
    private Color placeholderColor = new Color(160, 160, 160);
    private Color originalColor;
    private boolean empty;

    public void setFont(Font f) {
        super.setFont(f);
        if (empty) {
            originalFont = f;
        }
    }
    
    public PlaceholderTextField(String nome){
        this.setPlaceHolder(nome);
    }

    public void setForeground(Color c) {
        super.setForeground(c);
        if (empty) {
            originalColor = c;
        }
    }

    public Color getPlaceholderColor() {
        return placeholderColor;
    }

    public void setPlaceholderColor(Color placeholderColor) {
        this.placeholderColor = placeholderColor;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public void setPlaceHolder(final String text) {

        this.customizeText(text);

        this.getDocument().addDocumentListener(new DocumentListener() {

            public void normal() {
                if (getText().trim().length() != 0) {
                    setFont(originalFont);
                    setForeground(originalColor);
                    setEmpty(false);
                }

            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                normal();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                normal();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                normal();
            }
        });

        this.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (isEmpty()) {
                    setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (getText().trim().length() == 0) {
                    customizeText(text);
                }
            }

        });

    }
    private void customizeText(String text) {
        setText(text);
        setFont(new Font(getFont().getFamily(), Font.ITALIC, getFont().getSize()));
        setForeground(getPlaceholderColor());
        setEmpty(true);
    }

}
