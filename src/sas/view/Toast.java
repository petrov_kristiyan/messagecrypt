package sas.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import sas.view.studente.StudenteForm;

public class Toast extends JDialog {

    String msg;
    JFrame frame;

    public Toast(JFrame frame, JPanel panel, boolean modal, String msg) {
        super(frame, modal);
        this.msg = msg;
        this.frame = frame;
        setUndecorated(true);
        setModalityType(Dialog.ModalityType.MODELESS);
        initComponents();
        setVisible(true);
        setLocationRelativeTo(panel.getRootPane());

    }

    private void initComponents() {
        setLayout(new BorderLayout());
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                
                setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), 6, 6));
            }
        });
        
        int spacing = 5; 
        int width = 5;
        Container text = (Container) getLayeredPane().getComponents()[0];
        FontMetrics fm = text.getFontMetrics(text.getFont());
        width += spacing + fm.stringWidth(msg); 
        for (Component component : text.getComponents()) {
            width += (spacing + component.getWidth()); // pulsanti e spazio
        }
        width += spacing; // spazio in fondo
        width += getWidth() - getContentPane().getWidth(); // dialog bordi
        setSize(new Dimension(width, 50));
       
        Thread sleepThread = new Thread() {
            public void run() {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(StudenteForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                setVisible(false);
            }
        };
        sleepThread.start();
        getContentPane().setBackground(Color.BLACK);

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        final boolean isTranslucencySupported = gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT);

        if (isTranslucencySupported) {
            setOpacity(0.7f);
        }

        JLabel label = new JLabel();
        label.setForeground(Color.WHITE);
        label.setText(msg);
        add(label, BorderLayout.CENTER);
    }
}
