package sas.view.studente.messaggi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import sas.controller.studente.GuiController;
import sas.model.Observable;
import sas.model.Observer;
import sas.model.Studente;
import sas.model.messaggio.Messaggio;
import sas.model.messaggio.MessaggioDestinatario;
import sas.model.messaggio.MessaggioMittente;

public class Messaggi extends javax.swing.JPanel implements Observer, Observable {

    private final javax.swing.JList listaMessaggi;
    private final DefaultListModel model;
    private final JPanel main, cards;
    private final GuiController controller;
    private final Studente user;
    private List<MessaggioDestinatario> messaggiDestinatario;
    private List<MessaggioMittente> messaggiMittente;
    private MessaggioBozza messaggioBozza;
    private MessaggioInviato messaggioInviato;
    private MessaggioRicevuto messaggioRicevuto;
    private String tipoMessaggi;
    private final JLabel header;
    private final JScrollPane scrollPane;
    private final CardLayout cl;
    private boolean enter; //serve per sapere se un messaggio è stato aperto
    private Vector observersList;

    public Messaggi(GuiController controller, String tipoMessaggi, final Studente user) {
        initComponents();
        observersList = new Vector();
        cards = new JPanel(new CardLayout());
        cl = (CardLayout) cards.getLayout();
        this.user = user;
        header = new JLabel();
        header.setFont(new Font("Monospaced", Font.BOLD, 13));
        this.controller = controller;
        main = new JPanel();
        this.tipoMessaggi = tipoMessaggi;
        model = new DefaultListModel();
        listaMessaggi = new JList(model);
        listaMessaggi.setFont(new Font("Monospaced", Font.PLAIN, 13));
        scrollPane = new JScrollPane(main);
        listaMessaggi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    if (evt.getClickCount() == 2) {
                           if(model.getSize()>0){
                            apriMessaggio(evt);
                            notifyObservers("Enter");
                            enter = true;
                           }
                        }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(main, "Impossibile aprire il messaggio", "Errore di apertura", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(Messaggi.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        listaMessaggi.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof Messaggio) {
                    Messaggio messaggio = (Messaggio) value;
                    if (messaggio.getLetto() == 0 && messaggio.getDestinatario().getId() == user.getId()) {
                        setBorder(BorderFactory.createLineBorder(Color.yellow));
                    } else {
                        setBorder(null);
                    }
                }
                return c;
            }

        });
        this.setLayout(new BorderLayout());
        main.setLayout(new BorderLayout());
        main.add(header, BorderLayout.NORTH);
        main.add(listaMessaggi, BorderLayout.CENTER);
        cards.add(scrollPane, "Messaggi");
        add(cards, BorderLayout.CENTER);
    }

    public void apriMessaggio(MouseEvent e) throws SQLException, Exception {
        MessaggioMittente messaggioMittente;
        MessaggioDestinatario messaggioDestinatario;
        String show = "";
        if (listaMessaggi.getSelectedValue() instanceof StampaMessaggioMittente) {
            messaggioMittente = ((StampaMessaggioMittente) listaMessaggi.getSelectedValue()).getMex();
            messaggioMittente = controller.apriMessaggio(messaggioMittente.getId());
            if (tipoMessaggi.equals("Bozza")) {
                messaggioBozza = new MessaggioBozza(controller, user, messaggioMittente);
                messaggioBozza.register(this);
                show = "bozza";
                cards.add(messaggioBozza, show);

            } else {
                messaggioInviato = new MessaggioInviato(user, messaggioMittente);
                messaggioInviato.register(this);
                show = "inviato";
                cards.add(messaggioInviato, show);
                }
        } else {
                messaggioDestinatario = (MessaggioDestinatario) listaMessaggi.getSelectedValue();
                if (messaggioDestinatario.getDestinatario().getId() == user.getId()) {
                    messaggioDestinatario = controller.apriMessaggioRicevuto(messaggioDestinatario.getId());
                    messaggioRicevuto = new MessaggioRicevuto(user, messaggioDestinatario);
                    messaggioRicevuto.register(this);
                    show = "ricevuto";
                    cards.add(messaggioRicevuto, show);
                }
        }
        cl.show(cards, show);
    }

    public void updateList(String tipoMessaggio) {
        tipoMessaggi = tipoMessaggio;
        try {
            switch (tipoMessaggi) {
                case "Ricevuti":
                    messaggiDestinatario = controller.elencaMessaggiRicevuti();
                    break;
                case "Inviati":
                    messaggiMittente = controller.elencaMessaggiInviati();
                    break;
                case "Bozza":
                    messaggiMittente = controller.elencaMessaggiBozza();
                    break;
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Impossibile ottenere i messaggi", "Errore di lettura", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(Messaggi.class.getName()).log(Level.SEVERE, null, ex);
        }
        model.clear();
        if (messaggiDestinatario != null || messaggiMittente != null) {
            if (tipoMessaggi.equals("Ricevuti")) {
                String part1 = String.format("%-47s", "Mittente");
                header.setText((part1 + "Titolo"));
                messaggiDestinatario.stream().forEach((messaggio) -> {
                    model.addElement(messaggio);
                });
            } else {
                String part1 = String.format("%-47s", "Destinatario");
                header.setText((part1 + "Titolo"));
                messaggiMittente.stream().map((messaggio) -> new StampaMessaggioMittente(messaggio)).forEach((m) -> {
                    model.addElement(m);
                });
            }
        }
    }

    /**
     * Metodo utilizzato da MessaggioBozza/MessaggioInviato/MessaggioRicevuto
     * per tornare alla vecchia vista
     *
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public void showMessaggi() throws SQLException, Exception {
        cl.show(cards, "Messaggi");
        updateList(tipoMessaggi);
    }

    @Override
    public void update(Observable o, Object obj) {
        notifyObservers(obj);
        enter = false;
    }

    @Override
    public void notifyObservers(Object o) {
        for (int i = 0; i < observersList.size(); i++) {
            Observer observer = (Observer) observersList.elementAt(i);
            observer.update(this, o);
        }
    }

    @Override
    public void register(Observer o) {
        observersList.addElement(o);
    }

    /**
     * Classe creata per differenziare la stampa dei messaggi tra Ricevuti e
     * Inviati/Bozza
     */
    class StampaMessaggioMittente {

        private final Messaggio mex;

        public StampaMessaggioMittente(MessaggioMittente mex) {
            this.mex = (Messaggio) mex;
        }

        public String toString() {
            String part1 = String.format("%-47s", mex.getDestinatario().getUsername());
            return part1 + mex.getTitolo();
        }

        public Messaggio getMex() {
            return mex;
        }
    }

    public void eliminaMessaggi() throws SQLException {
        Messaggio selected;
        StampaMessaggioMittente mex;
        int[] index = listaMessaggi.getSelectedIndices();
        for (int i = index.length - 1; i >= 0; i--) {
            if (tipoMessaggi.equals("Ricevuti")) {
                selected = (Messaggio) listaMessaggi.getModel().getElementAt(index[i]);
                controller.eliminaMessaggi(selected);
            } else {
                mex = (StampaMessaggioMittente) listaMessaggi.getModel().getElementAt(index[i]);
                controller.eliminaMessaggi(mex.getMex());
            }
            ((DefaultListModel) listaMessaggi.getModel()).removeElementAt(index[i]);

        }
    }

    public boolean isEnter() {
        return enter;
    }

    public void setEnter(boolean enter) {
        this.enter = enter;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
