package sas.view.studente.messaggi;

import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import sas.controller.studente.GuiController;
import sas.model.Observable;
import sas.model.Observer;
import sas.model.Studente;
import sas.model.UserInfo;
import sas.model.messaggio.Messaggio;
import sas.model.messaggio.MessaggioMittente;
import sas.view.Toast;

public class MessaggioBozza extends javax.swing.JPanel implements Observable{

    private GuiController controller;
    private UserInfo user;
    private MessaggioMittente messaggio;
    private Vector observersList;

    public MessaggioBozza(GuiController controller, Studente user, MessaggioMittente messaggio) throws SQLException {
        initComponents();
        observersList = new Vector();
        this.user = new UserInfo(user.getId(), user.getNome(), user.getCognome(), user.getUsername());
        this.controller = controller;
        this.messaggio = messaggio;
        mittente.setText(messaggio.getMittente().getUsername());
        destinatario.setModel(new DefaultComboBoxModel(controller.elencaDestinatari().toArray()));
        lingua.setModel(new DefaultComboBoxModel(new String[]{"Italiano", "Inglese"}));
        destinatario.setSelectedItem(messaggio.getDestinatario());
        lingua.setSelectedItem(messaggio.getLingua());
        testo.setText(messaggio.getTesto());
        testo.setWrapStyleWord(true);
        testo.setLineWrap(true);
        titolo.setText(messaggio.getTitolo());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        indietro = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        testo = new javax.swing.JTextArea();
        mittente = new javax.swing.JLabel();
        destinatario = new javax.swing.JComboBox();
        lingua = new javax.swing.JComboBox();
        titolo = new javax.swing.JTextField();
        salva = new javax.swing.JButton();
        invia = new javax.swing.JButton();

        indietro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/go_back_256.png"))); // NOI18N
        indietro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                indietroMouseClicked(evt);
            }
        });

        testo.setColumns(20);
        testo.setRows(5);
        jScrollPane1.setViewportView(testo);

        mittente.setBorder(javax.swing.BorderFactory.createTitledBorder("Mittente"));

        titolo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Titolo", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        salva.setText("Salva");
        salva.setToolTipText("");
        salva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvaActionPerformed(evt);
            }
        });

        invia.setText("Invia");
        invia.setToolTipText("");
        invia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inviaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(salva, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(invia, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(indietro, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                        .addComponent(titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                        .addComponent(lingua, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mittente, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(destinatario, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(indietro, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lingua, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(titolo, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mittente, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(destinatario, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(salva)
                    .addComponent(invia))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void indietroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_indietroMouseClicked
        Messaggi old = (Messaggi) getParent().getParent();//main panel di Messaggi
        notifyObservers("Bozza");
        try {
            old.showMessaggi();
        } catch (SQLException ex) {
            JOptionPane.showInputDialog(ex);
            Logger.getLogger(MessaggioBozza.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MessaggioBozza.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_indietroMouseClicked

    private void salvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvaActionPerformed
        if (destinatario.getSelectedItem() != null && !titolo.getText().isEmpty() && !testo.getText().isEmpty()) {
            messaggio = new Messaggio(testo.getText(), (String) lingua.getSelectedItem(), titolo.getText(), true, false, user, (UserInfo) destinatario.getSelectedItem());
            try {
                messaggio.setId(this.messaggio.getId());
                messaggio.cifra();
                messaggio.salva();
                JDialog dialog = new Toast(null, this, true, "  Salvato in bozze!");

            } catch (SQLException ex) {
                Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);

                JOptionPane.showMessageDialog(this, ex + "Impossibile salvare il messaggio in bozze.", "Errore salvataggio in bozze", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                Logger.getLogger(MessaggioBozza.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_salvaActionPerformed

    private void inviaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inviaActionPerformed
        if (destinatario.getSelectedItem() != null && !testo.getText().isEmpty() && !titolo.getText().isEmpty()) {
            messaggio = new Messaggio(testo.getText(), (String) lingua.getSelectedItem(), titolo.getText(), false, true, user, (UserInfo) destinatario.getSelectedItem());
            try {
                messaggio.cifra();
                controller.spedisciMessaggio((Messaggio) messaggio);
                JDialog dialog = new Toast(null, this, true, "  Messaggio inviato!");
            } catch (SQLException ex) {
                Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, "Impossibile cifrare il messaggio.", "Errore cifratura", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                Logger.getLogger(MessaggioBozza.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_inviaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox destinatario;
    private javax.swing.JLabel indietro;
    private javax.swing.JButton invia;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox lingua;
    private javax.swing.JLabel mittente;
    private javax.swing.JButton salva;
    private javax.swing.JTextArea testo;
    private javax.swing.JTextField titolo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void notifyObservers(Object o) {
        for (int i = 0; i < observersList.size(); i++) {
            Observer observer = (Observer) observersList.elementAt(i);
            observer.update(this,o);
        }
    }

    @Override
    public void register(Observer o) {
        observersList.addElement(o);
    }
}
