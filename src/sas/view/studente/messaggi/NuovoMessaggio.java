/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas.view.studente.messaggi;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import sas.controller.studente.GuiController;
import sas.model.Studente;
import sas.model.UserInfo;
import sas.model.messaggio.Messaggio;
import sas.view.PlaceholderTextField;
import sas.view.Toast;


public class NuovoMessaggio extends javax.swing.JPanel {

    private final GuiController controller;
    private Messaggio messaggio;
    private JTextArea testo;
    private final JScrollPane scrollTesto;
    private final JButton salvaBozzaMessaggio;
    private final JButton inviaMessaggio;
    private final JPanel opzioni;
    private final PlaceholderTextField titolo;
    private final JPanel dettagliMessaggio;
    private final JComboBox lingua, destinatario;
    private final UserInfo user;

    public NuovoMessaggio(Studente user, GuiController controller) {
        this.controller = controller;
        initComponents();
        messaggio = null;
        this.user = new UserInfo(user.getId(), user.getNome(), user.getCognome(), user.getUsername());
        this.setLayout(new BorderLayout());
        testo = new JTextArea();
        scrollTesto = new JScrollPane();
        salvaBozzaMessaggio = new JButton();
        inviaMessaggio = new JButton();
        dettagliMessaggio = new JPanel();
        titolo = new PlaceholderTextField("Oggetto :");
        destinatario = new JComboBox();
        lingua = new JComboBox();
        try {
            destinatario.setModel(new DefaultComboBoxModel(controller.elencaDestinatari().toArray()));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Impossibile ottenere i destinatari.", "Errore Destinatari", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);
        }
        lingua.setModel(new DefaultComboBoxModel(new String[]{"Italiano", "Inglese"}));
        salvaBozzaMessaggio.addActionListener((ActionEvent evt) -> {
            try {
                salvaMessaggioBozza(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(dettagliMessaggio, "Impossibile salvare il messaggio come bozza", "Errore di salvataggio", JOptionPane.ERROR_MESSAGE);
                System.out.print(ex + "errore salva messaggi bozza");
            }
        });
        inviaMessaggio.addActionListener((ActionEvent evt) -> {
            try {
                inviaMessaggio(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(dettagliMessaggio, "Impossibile inviare il messaggio", "Errore di comunicazione", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        opzioni = new JPanel(new BorderLayout());
        testo.setWrapStyleWord(true);
        testo.setLineWrap(true);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                testo.requestFocus();
            }
        });
        scrollTesto.setViewportView(testo);
        salvaBozzaMessaggio.setText("Salva come Bozza");
        inviaMessaggio.setText("Invia");
        GroupLayout layout = new GroupLayout(dettagliMessaggio);
        dettagliMessaggio.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(destinatario)
                .addGroup(layout.createBaselineGroup(false, true)
                        .addComponent(titolo)
                        .addComponent(lingua)
                )
        );
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addComponent(destinatario)
                        .addComponent(titolo)
                )
                .addComponent(lingua)
        );

        dettagliMessaggio.add(titolo, BorderLayout.PAGE_START);
        dettagliMessaggio.add(lingua, BorderLayout.EAST);
        dettagliMessaggio.add(destinatario, BorderLayout.WEST);
        add(dettagliMessaggio, BorderLayout.PAGE_START);
        add(scrollTesto, BorderLayout.CENTER);
        opzioni.add(salvaBozzaMessaggio, BorderLayout.WEST);
        opzioni.add(inviaMessaggio, BorderLayout.EAST);
        add(opzioni, BorderLayout.PAGE_END);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    private void inviaMessaggio(ActionEvent e) throws Exception {
        if (destinatario.getSelectedItem() != null && !testo.getText().isEmpty() && !titolo.getText().isEmpty()) {
            messaggio = new Messaggio(testo.getText(), (String) lingua.getSelectedItem(), titolo.getText(), false, false, user, (UserInfo) destinatario.getSelectedItem());
            try {
                messaggio.cifra();
                controller.spedisciMessaggio(messaggio);
                JDialog dialog = new Toast(null, this, true, "  Messaggio inviato!");

            } catch (SQLException ex) {
                Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, "Impossibile cifrare il messaggio.", "Errore cifratura", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void salvaMessaggioBozza(ActionEvent e) throws Exception {
        if (!titolo.getText().isEmpty() && !testo.getText().isEmpty()) {
            messaggio = new Messaggio(testo.getText(), (String) lingua.getSelectedItem(), titolo.getText(), true, false, user, (UserInfo) destinatario.getSelectedItem());
            try {
                messaggio.cifra();
                controller.salvaMessaggioBozza(messaggio);
                JDialog dialog = new Toast(null, this, true, "  Salvato in bozze!");
            } catch (SQLException ex) {
                Logger.getLogger(NuovoMessaggio.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, "Impossibile salvare il messaggio in bozze.", "Errore salvataggio in bozze", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
