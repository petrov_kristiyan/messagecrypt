package sas.view.studente.sistemaDiCifratura;

import java.awt.BorderLayout;
import java.awt.Font;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import sas.controller.studente.GuiController;
import sas.model.SistemaDiCifratura;
import sas.view.Toast;

public class ListaSistemiDiCifratura extends javax.swing.JPanel {

    private final GuiController controller;
    private JList listaSistemi;
    private List<SistemaDiCifratura> lista;
    private final JLabel header;
    private final DefaultListModel model;

    public ListaSistemiDiCifratura(GuiController controller) {
        initComponents();
        this.controller = controller;
        model = new DefaultListModel();
        listaSistemi = new JList(model);
        String part1 = String.format("%-47s", "Chiave");
        header = new JLabel(part1 + "Metodo");
        header.setFont(new Font("Monospaced", Font.BOLD, 13));
        listaSistemi.setFont(new Font("Monospaced", Font.PLAIN, 13));
        listaSistemi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    if(model.getSize()>0)
                    ((SistemaDiCifraturaView) getParent().getParent()).inviaProposta((SistemaDiCifratura) listaSistemi.getSelectedValue());
                }
            }
        });
        add(header, BorderLayout.NORTH);
        add(listaSistemi, BorderLayout.CENTER);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    public void updateList() {
        try {
            this.lista = controller.elencaSistemaDiCifratura();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Impossibile ottenere i sistemi di ciratura.", "Errore sistema di cifratura", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ListaSistemiDiCifratura.class.getName()).log(Level.SEVERE, null, ex);
        }
        model.clear();
        lista.stream().filter((sistema) -> (sistema.getChiave() != null)).forEach((sistema) -> {
            model.addElement(sistema);
        });
    }

    public void eliminaSistemi() {
        try {
            int[] index = listaSistemi.getSelectedIndices();
            for (int i = index.length - 1; i >= 0; i--) {
                SistemaDiCifratura selected = (SistemaDiCifratura) listaSistemi.getModel().getElementAt(index[i]);
                controller.eliminaSistemaDiCifratura(selected);
                ((DefaultListModel) listaSistemi.getModel()).removeElementAt(index[i]);
                JDialog dialog = new Toast(null, this, true, "  Eliminazione effettuata!");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Errore eliminazione.", "Errore eliminazione", JOptionPane.ERROR_MESSAGE);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
