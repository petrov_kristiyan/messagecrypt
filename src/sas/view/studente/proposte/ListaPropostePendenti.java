/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas.view.studente.proposte;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import sas.controller.studente.GuiController;
import sas.model.Proposta;

/**
 *
 * @author kristiyan
 */
public class ListaPropostePendenti extends javax.swing.JPanel {

    private final GuiController controller;
    private JList listaProposte;
    private final JLabel header;
    private List<Proposta> lista;
    private  DefaultListModel model;

    public ListaPropostePendenti(GuiController controller)  {
        initComponents();
        this.controller = controller;
        model = new DefaultListModel();
        listaProposte = new JList(model);
        try {
            lista = controller.vediProposteSistemaDiCifratura();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Impossibile ottenere le proposte", "Errore di caricamento", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ListaPropostePendenti.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (lista != null) {
            lista.stream().forEach((prop) -> {
                model.addElement(prop);
            });

        }
        String part1 = String.format("%-20s", "Partner");
        String part2 = String.format("%-20s", "Proponente");
        String part3 = String.format("%-20s", "Chiave");
        String part4 = String.format("%-20s", "Metodo");
        header = new JLabel(part2 + part1 +part3+ part4 + "Stato");
        header.setFont(new Font("Monospaced", Font.BOLD, 13));
        listaProposte.setFont(new Font("Monospaced", Font.PLAIN, 13));
        add(header, BorderLayout.NORTH);
        add(listaProposte, BorderLayout.CENTER);
        listaProposte.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    if (model.getSize()>0 &&evt.getClickCount() == 2) {
                        apriProposta();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Impossibile aprire la proposta", "Errore apertura", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(ListaPropostePendenti.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void apriProposta() throws Exception {
        Object[] possibilities = {"rifiuta", "accetta"};
        Proposta p = (Proposta) listaProposte.getSelectedValue();
        String s = (String) JOptionPane.showInputDialog(this, "Accetta o Rifiuta la Proposta\n" + p, "Accetta o Rifiuta Proposta", JOptionPane.PLAIN_MESSAGE, null, possibilities, "rifiuta");
        if ((s != null)) {
            if(controller.comunicaDecisione(p, s)){
                updateList();
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    public void updateList() throws Exception {
        model.clear();
        lista = controller.vediProposteSistemaDiCifratura();
        if (lista != null) {
            lista.stream().forEach((prop) -> {
                model.addElement(prop);
            });

        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
