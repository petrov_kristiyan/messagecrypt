/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sas.view.spia;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import sas.controller.Controller;
import sas.controller.spia.GuiControllerSpia;
import sas.model.AlberoIpotesi;
import sas.model.Ipotesi;
import sas.model.Sessione;
import sas.model.frequenze.Frequenza;
import sas.view.Toast;
import sas.view.studente.StudenteForm;

public final class SessioneView extends javax.swing.JPanel implements Serializable {

    private JTable tabellaAlfabeto, tabellaAlfabetoCalcolato;
    private Frequenza freq;
    private GuiControllerSpia controller;
    private String testo="", testoIpotetico;
    private Sessione sessione;
    private AlberoIpotesi albero;
    private JPanel cards;
    private boolean aggiorna;
    private static int timestamp = 0;
    private List<Ipotesi> listaM2;

    public SessioneView(Sessione sessione, GuiControllerSpia controller) throws Exception {
        initComponents();

        aggiorna = false;

        this.controller = controller;

        listaM2 = new ArrayList<>();

        if (sessione == null) {
            sessione = controller.creaSessione();
        }
        this.sessione = sessione;

        this.albero = sessione.getAlbero();

        timestamp = albero.getMaxTimestamp();
        if(sessione.getMessaggioCifrato() !=null )
        this.testo = sessione.getMessaggioCifrato().getTestoCifrato();
        if (testo.equals("")) {
            testo = " ";
            for (Component component : getComponents()) {
                component.setEnabled(false);
            }
            jSeparator1.setVisible(false);
            salva.setVisible(false);
        }
        testoCifrato.setText(testo);
        freq = Frequenza.richiediFrequenza((String) frequenza.getSelectedItem());
        Map<String, Double> frequenzaAlfabeto = freq.frequenza((String) lingua.getSelectedItem());
        tabellaAlfabeto = new JTable(popolaTabella(frequenzaAlfabeto));
        personalizzaTabella(tabellaAlfabeto);
        jScrollPane3.setViewportView(tabellaAlfabeto);   
        Map<String, Double> freqContate = freq.frequenzaCalcolata(testo);
        testoIpotetico = sessione.getSoluzione().getTestoIpotizzato();
        testoIpotizzato.setText(testoIpotetico);
        tabellaAlfabetoCalcolato = new JTable(popolaTabella(freqContate));
        personalizzaTabella(tabellaAlfabetoCalcolato);
        jScrollPane4.setViewportView(tabellaAlfabetoCalcolato);

    }

    public DefaultTableModel popolaTabella(Map<String, Double> freq) {
        freq = sortByValueDESC(freq);
        Map newMap;
        if (freq.size() > 30) {
            newMap = Frequenza.trimFreq(freq);
        } else {
            newMap = freq;
        }
        Object[][] frequenze = new Object[1][newMap.values().size()];
        System.arraycopy(newMap.values().toArray(), 0, frequenze[0], 0, newMap.values().size());
        DefaultTableModel model = new DefaultTableModel(frequenze, newMap.keySet().toArray()) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return model;
    }

    public void personalizzaTabella(JTable tabella) {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 0; i < tabella.getColumnCount(); i++) {
            tabella.setDefaultRenderer(tabella.getColumnClass(i), renderer);
        }
        DefaultTableCellRenderer rend = (DefaultTableCellRenderer) tabella.getTableHeader().getDefaultRenderer();
        rend.setHorizontalAlignment(JLabel.CENTER);
        tabella.getTableHeader().setFont(new Font("Monospaced", Font.PLAIN, 16));
        tabella.getTableHeader().setReorderingAllowed(false);
        tabella.setRowHeight(20);
        tabella.setIntercellSpacing(new Dimension(0, 0));
        tabella.getTableHeader().setResizingAllowed(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        testoCifrato = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        testoIpotizzato = new javax.swing.JTextArea();
        lettere = new javax.swing.JTextField();
        assegnamento = new javax.swing.JTextField();
        mappa = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        lingua = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jSeparator2 = new javax.swing.JSeparator();
        salva = new javax.swing.JLabel();
        studente = new javax.swing.JLabel();
        esci = new javax.swing.JLabel();
        frequenza = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        undo = new javax.swing.JButton();
        indietro = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();

        jScrollPane1.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("Messaggio Cifrato"));

        testoCifrato.setColumns(20);
        testoCifrato.setRows(5);
        testoCifrato.setBorder(null);
        testoCifrato.setEnabled(false);
        jScrollPane1.setViewportView(testoCifrato);
        testoCifrato.setWrapStyleWord(true);
        testoCifrato.setLineWrap(true);

        jScrollPane2.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("Testo Ipotizzato"));

        testoIpotizzato.setColumns(20);
        testoIpotizzato.setRows(5);
        testoIpotizzato.setEnabled(false);
        jScrollPane2.setViewportView(testoIpotizzato);
        testoIpotizzato.setWrapStyleWord(true);
        testoIpotizzato.setLineWrap(true);

        lettere.setToolTipText("");

        mappa.setText("Mappa");
        mappa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mappaActionPerformed(evt);
            }
        });

        lingua.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Italiano", "Inglese" }));
        lingua.setBorder(javax.swing.BorderFactory.createTitledBorder("Lingua"));

        jScrollPane3.setBorder(javax.swing.BorderFactory.createTitledBorder("Frequenze nella lingua"));

        jScrollPane4.setBorder(javax.swing.BorderFactory.createTitledBorder("Frequenze dei caratteri del testo cifrato"));

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        salva.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        salva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/save.png"))); // NOI18N
        salva.setToolTipText("Salva");
        salva.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Salva", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        salva.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salvaMouseClicked(evt);
            }
        });

        studente.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        studente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/studente.png"))); // NOI18N
        studente.setToolTipText("Entra come Studente");
        studente.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Studente", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        studente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                studenteMouseClicked(evt);
            }
        });

        esci.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        esci.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/logout.png"))); // NOI18N
        esci.setToolTipText("Esci");
        esci.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Esci", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        esci.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                esciMouseClicked(evt);
            }
        });

        frequenza.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Singola", "Digrammi" }));
        frequenza.setToolTipText("");
        frequenza.setBorder(javax.swing.BorderFactory.createTitledBorder("Frequenza"));

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabel1.setText("-->");

        undo.setText("Undo");
        undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoActionPerformed(evt);
            }
        });

        indietro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        indietro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/go_back_256.png"))); // NOI18N
        indietro.setToolTipText("Indietro");
        indietro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Indietro", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        indietro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                indietroMouseClicked(evt);
            }
        });
        indietro.setVisible(false);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sas/view/assets/list.png"))); // NOI18N
        jLabel2.setToolTipText("Visualizza Ipotesi");
        jLabel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Ipotesi", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lingua, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(240, 240, 240)
                        .addComponent(frequenza, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane4)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane1)
                                        .addGap(17, 17, 17))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lettere, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(assegnamento, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(mappa, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(46, 46, 46)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                                    .addComponent(undo, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)))
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studente, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(esci, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salva, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(indietro, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lingua, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(frequenza, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lettere, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(assegnamento, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mappa, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(undo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(indietro, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salva, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studente, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(esci, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addComponent(jSeparator2)))
        );

        lingua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateFreq();
            }
        });
        frequenza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateFreq();
            }
        });
        jSeparator3.setVisible(false);
    }// </editor-fold>//GEN-END:initComponents

    private void studenteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_studenteMouseClicked
        try {
            controller.shiftUser();
        } catch (SQLException ex) {
            Logger.getLogger(StudenteForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_studenteMouseClicked

    private void esciMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_esciMouseClicked
        Controller.getInstance().logout();
    }//GEN-LAST:event_esciMouseClicked

    private void mappaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mappaActionPerformed
        if (!lettere.getText().isEmpty() && !assegnamento.getText().isEmpty() && lettere.getText().length() == assegnamento.getText().length()) {
            for (int i = 0; i < lettere.getText().length(); i++) {
                Character lettera = lettere.getText().charAt(i);
                Character assegnata = assegnamento.getText().charAt(i);
                //se esiste la chiave nel albero delle ipotesi
                if (albero.getAlbero().containsKey(lettera)) {
                    //lista delle Mosse associata alla chiave
                    List<Ipotesi> listaMosse = albero.getAlbero().get(lettera);
                    Ipotesi mossa = new Ipotesi(assegnata);
                    int index = albero.lastIndexOf(listaMosse, mossa); // se esiste mossa dammi l'indice dell'ultima
                    if ( index >= 0 ) {
                        mossa = listaMosse.get(index);
                        if (mossa.isOperazione() == false) { //se la mossa è undo 
                            int n = JOptionPane.showConfirmDialog(this, "Stai tornando sui tuoi passi... Questo assegnamento lo avevi fatto prima di fare undo sei sicuro di volerlo rifare?", "REDO mappatura", JOptionPane.YES_NO_OPTION);
                            if (n == JOptionPane.YES_OPTION) {
                                Object[] options = {"Annulla", "Elimina Mosse selezionate"};
                                String nome = (String) JOptionPane.showInputDialog(null, "Vuoi dare un nome a questa assegnazione?", "Salva con nome", JOptionPane.INFORMATION_MESSAGE, null, null, "");
                                timestamp++;
                                mossa = listaMosse.get(listaMosse.size() - 1);
                                mossa.setOperazione(false);
                                listaMosse.set(listaMosse.size() - 1, mossa);
                                Ipotesi ipotesi = new Ipotesi(assegnata, true, timestamp, nome);
                                albero.aggiungiIpotesi(lettera, listaMosse, ipotesi );
                                testoIpotetico = testoIpotetico.replace(lettera, assegnata);
                                testoIpotizzato.setText(testoIpotetico);
                            }
                        } else { //è una mossa di assegnazione
                            JOptionPane.showMessageDialog(this, "Questa mossa esiste già !!", "Errore mappatura", JOptionPane.ERROR_MESSAGE);
                        }
                    } else { // quest assegnazione non esiste, ma esiste un altra
                        mossa = listaMosse.get(listaMosse.size() - 1);
                        if (mossa.isOperazione()) { //se è un assegnazione , chiedi
                            int n = JOptionPane.showConfirmDialog(this, "Hai già effettuato una mappatura con questo carattere\n"
                                    + "Vuoi sostituire la mappatura?\n"
                                    + lettera + " ==> " + mossa.getAssegnamento() + ""
                                    + "\ncon\n"
                                    + lettera + " ==> " + assegnata + "", "Sovvrascrivi mappatura", JOptionPane.YES_NO_OPTION);
                            if (n == JOptionPane.YES_OPTION) {
                                timestamp++;
                                mossa.setOperazione(false);
                                listaMosse.set(listaMosse.size() - 1, mossa);
                                String nome = (String) JOptionPane.showInputDialog(null, "Vuoi dare un nome a questa assegnazione?", "Salva con nome", JOptionPane.INFORMATION_MESSAGE, null, null, "");
                                Ipotesi ipotesi = new Ipotesi(assegnata, true, timestamp, nome);     
                                albero.aggiungiIpotesi(lettera, listaMosse, ipotesi );
                                testoIpotetico = testoIpotetico.replace(lettera, assegnata);
                                testoIpotizzato.setText(testoIpotetico);
                            }
                        } else { //se è un undo, memorizza
                            List<Ipotesi> m = new ArrayList();
                            timestamp++;
                            String nome = (String) JOptionPane.showInputDialog(null, "Vuoi dare un nome a questa assegnazione?", "Salva con nome", JOptionPane.INFORMATION_MESSAGE, null, null, "");
                            Ipotesi ipotesi = new Ipotesi(assegnata, true, timestamp, nome);
                            albero.aggiungiIpotesi(lettera, listaMosse, ipotesi );
                            testoIpotetico = testoIpotetico.replace(lettera, assegnata);
                            testoIpotizzato.setText(testoIpotetico);
                        }
                    }
                } else {//chiave non esistente
                    List<Ipotesi> m = new ArrayList();
                    timestamp++;
                    String nome = (String) JOptionPane.showInputDialog(null, "Vuoi dare un nome a questa assegnazione?", "Salva con nome", JOptionPane.INFORMATION_MESSAGE, null, null, "");
                    Ipotesi ipotesi = new Ipotesi(assegnata, true, timestamp, nome);
                    albero.aggiungiIpotesi(lettera, m, ipotesi );
                    testoIpotetico = testoIpotetico.replace(lettera, assegnata);
                    testoIpotizzato.setText(testoIpotetico);
                }
            }
        } else {
            JDialog dialog = new Toast(null, this, true, "  Errore mappatura, controllare l'input.");
        }
    }//GEN-LAST:event_mappaActionPerformed

    private void undoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoActionPerformed
        JPanel panelUndo = new JPanel();
        DefaultListModel model = new DefaultListModel();
        JList lista = new JList();
        JLabel header = new JLabel();
        header.setFont(new Font("Monospaced", Font.BOLD, 13));
        JScrollPane scrollPanel = new JScrollPane();
        panelUndo.setLayout(new BorderLayout());
        String part1 = String.format("%-23s", "#");
        String part2 = String.format("%-23s", "Nome");
        String part3 = String.format("%-23s", "Carattere");
        header.setText(part1 + part2 + part3 + "Assegnamento");
        panelUndo.add(header, BorderLayout.NORTH);
        lista.setFont(new Font("Monospaced", Font.PLAIN, 13));
        //mostra la lista delle mosse effettuate
        listaM2 = albero.getMosseOrdinate();
        listaM2.forEach(e -> {
            Ipotesi m = listaM2.get(albero.firstIndexOf(listaM2, e));
            if (m.isOperazione() && e.getTimestamp() == m.getTimestamp()) {
                model.addElement(new Ipotesi(e.getTimestamp(), e.getNome(), e.getChiave(), e.getAssegnamento(), e.isOperazione()));
            }
        });
        System.out.println();
        lista.setModel(model);
        scrollPanel.setViewportView(lista);
        panelUndo.add(scrollPanel, BorderLayout.CENTER);
        Object[] options = {"Annulla", "Elimina Mosse selezionate"};
        int n = JOptionPane.showOptionDialog(null, panelUndo, "UNDO", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, null);
        if (n == 1) {
            int[] index = lista.getSelectedIndices();
            for (int k = index.length - 1; k >= 0; k--) {
                Ipotesi mossa = (Ipotesi) lista.getModel().getElementAt(index[k]);
                timestamp++;
                Ipotesi mossa2 = new Ipotesi(timestamp, mossa.getNome(), mossa.getChiave(), mossa.getAssegnamento(), false);
                albero.undoIpotesi(mossa.getChiave(),mossa2);
                testoIpotetico = testo.replace(mossa2.getAssegnamento(), mossa2.getChiave());
                testoIpotizzato.setText(testoIpotetico);
                ((DefaultListModel) lista.getModel()).removeElementAt(index[k]);
            }
        }
    }//GEN-LAST:event_undoActionPerformed

    private void salvaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salvaMouseClicked
        String nome = (String) JOptionPane.showInputDialog(null, "Inserisci il nome con cui vuoi salvare la sessione ", "Salva con nome", JOptionPane.INFORMATION_MESSAGE, null, null, sessione.getNome());
        if (nome!=null && !nome.equals("")) {
            try {
                sessione.setAlbero(albero);
                sessione.getSoluzione().setTestoIpotizzato(testoIpotizzato.getText());
                sessione.setNome(nome);
                if (controller.salvaSessione(sessione, aggiorna)) {
                    JDialog dialog = new Toast(null, this, true, "  Sessione salvata");
                    aggiorna = true;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Impossibile salvare la sessione.", "Errore salvataggio", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(SessioneView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
           JOptionPane.showMessageDialog(this, "Impossibile salvare la sessione senza un nome.", "Errore salvataggio", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_salvaMouseClicked

    private void indietroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_indietroMouseClicked
        CardLayout cl = (CardLayout) cards.getLayout();
        cl.show(cards, "Lista");
    }//GEN-LAST:event_indietroMouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        JPanel panel = new JPanel();
        DefaultListModel model = new DefaultListModel();
        JList lista = new JList();
        JLabel header = new JLabel();
        header.setFont(new Font("Monospaced", Font.BOLD, 13));
        JScrollPane scrollPanel = new JScrollPane();
        panel.setLayout(new BorderLayout());
        String part1 = String.format("%-17s", "#");
        String part2 = String.format("%-17s", "Nome");
        String part3 = String.format("%-17s", "Carattere");
        String part4 = String.format("%-17s", "Assegnamento");
        header.setText(part1 + part2 + part3 + part4 + "Operazione");
        panel.add(header, BorderLayout.NORTH);
        lista.setFont(new Font("Monospaced", Font.PLAIN, 13));
        listaM2 = albero.getMosseOrdinate();
        listaM2.forEach(e -> {
            String part5 = String.format("%-17s", e.getTimestamp());
            String part6 = String.format("%-17s", e.getNome());
            String part7 = String.format("%-17s", e.getChiave());
            String part8 = String.format("%-17s", e.getAssegnamento());
            if (e.isOperazione()) {
                model.addElement(part5 + part6 + part7 + part8 + "mossa");
            } else {
                model.addElement(part5 + part6 + part7 + part8 + "undo");
            }
        });
        lista.setModel(model);
        scrollPanel.setViewportView(lista);
        panel.add(scrollPanel, BorderLayout.CENTER);
        JOptionPane.showMessageDialog(null, panel, "Ipotesi", JOptionPane.PLAIN_MESSAGE);
    }//GEN-LAST:event_jLabel2MouseClicked
    public static <String, Double extends Comparable<? super Double>> Map<String, Double> sortByValueDESC(Map<String, Double> map) {
        Map<String, Double> result = new LinkedHashMap<>();
        Stream<Entry<String, Double>> st = map.entrySet().stream();
        st.sorted((Comparator.comparing(e -> e.getValue(), Comparator.reverseOrder()))).forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

    public void updateFreq() {
        freq = Frequenza.richiediFrequenza((String) frequenza.getSelectedItem());
        Map<String, Double> frequenzaAlfabeto = freq.frequenza((String) lingua.getSelectedItem());
        tabellaAlfabeto.setModel(popolaTabella(frequenzaAlfabeto));
        Map<String, Double> freqContate = freq.frequenzaCalcolata(testo);
        tabellaAlfabetoCalcolato.setModel(popolaTabella(freqContate));
    }

    public void showIndietro(JPanel cards) {
        indietro.setVisible(true);
        jSeparator3.setVisible(true);
        aggiorna = true;
        this.cards = cards;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField assegnamento;
    private javax.swing.JLabel esci;
    private javax.swing.JComboBox frequenza;
    private javax.swing.JLabel indietro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JTextField lettere;
    private javax.swing.JComboBox lingua;
    private javax.swing.JButton mappa;
    private javax.swing.JLabel salva;
    private javax.swing.JLabel studente;
    private javax.swing.JTextArea testoCifrato;
    private javax.swing.JTextArea testoIpotizzato;
    private javax.swing.JButton undo;
    // End of variables declaration//GEN-END:variables
}
