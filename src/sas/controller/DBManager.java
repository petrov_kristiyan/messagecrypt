package sas.controller;
import java.sql.*;
import com.sun.rowset.CachedRowSetImpl;
import javax.sql.rowset.CachedRowSet;
/**
 * Classe che si occupa del accesso al DB 
 * e delle query
 */
public class DBManager {
    
    private static final String url = "jdbc:derby://localhost:1527/sample";
    private static final String user = "app";
    private static final String pass = "app";
    
    /**
     * Metodo usato per ottenere il risultato della query ( SELECT ) salvandosi in cache 
     * il result set per non perdere il suo valore
     * 
     * @param query Stringa di operazioni da eseguire tramite sql
     * @return ritorna il resultset della query
     * @throws SQLException errore lato DB
     */
    public static CachedRowSet executeQuery  (String query) throws SQLException {
        Connection con = DriverManager.getConnection(url, user, pass);
        Statement st = con.createStatement();
        CachedRowSet cachedRowSet= new CachedRowSetImpl(); 
        ResultSet rs=st.executeQuery(query);
        cachedRowSet.populate(rs);
        st.close();
        con.close();  
        return cachedRowSet;
    }
    /**
     * Metodo usato per ottenere il risultato della query ( UPDATE ) rirtorna 0 o 1
     * 
     * @param query Stringa di operazioni da eseguire tramite sql
     * @return 0 se l'operazione è andata a buon fine 1 altrimenti
     * @throws SQLException errore lato DB
     */
    public static int executeUpdate (String query)throws SQLException {
        Connection con = DriverManager.getConnection(url,user,pass);
        Statement st = con.createStatement();
        int a = st.executeUpdate(query);
        st.close();
        con.close();     
        return a;
    }
    public static PreparedStatement createStatement(String query) {
        Connection con;
        PreparedStatement ps = null;
        try {
            con = DriverManager.getConnection(url,user,pass);
            ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ps;
    } 
}