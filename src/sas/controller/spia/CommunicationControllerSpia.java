package sas.controller.spia;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import sas.model.Sessione;
import sas.model.Spia;

public class CommunicationControllerSpia {

    public boolean salvaSessione(Sessione sessione) throws SQLException, IOException{
       return sessione.salva();
    }
    
    public Sessione creaSessione(Spia spia) throws Exception{
        return new Sessione(spia);
    }
    public List<Sessione> elencaSessioni(Spia spia) throws SQLException, IOException, ClassNotFoundException{
        return Sessione.elencaSessioni(spia);
    }
    public boolean aggiornaSessione(Sessione sessione) throws IOException, SQLException{
        return sessione.aggiornaSessione();
    }
    public boolean eliminaSessione(Sessione sessione) throws SQLException{
        return sessione.eliminaSessione();
    }
}
