package sas.controller.spia;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import sas.controller.UserFrame;
import sas.controller.studente.GuiController;
import sas.model.Ipotesi;
import sas.model.Sessione;
import sas.model.Spia;
import sas.model.Studente;
import sas.view.spia.SpiaForm;

public class GuiControllerSpia extends UserFrame {

    private final SpiaForm spiaForm;
    private static GuiControllerSpia instance;
    private static CommunicationControllerSpia cmm;
    private Spia spia;

    public GuiControllerSpia() {
        spia = new Studente(user.getNome(), user.getCognome(), user.getId(), user.getUsername(), user.getPwd());
        spiaForm = new SpiaForm().getInstance(this);
        generateFrame(spiaForm);
        cmm = new CommunicationControllerSpia();
    }

    public static GuiControllerSpia getInstance() {
        if (instance == null) {
            instance = new GuiControllerSpia();
        }
        return instance;
    }

    public boolean salvaSessione(Sessione sessione, boolean aggiorna) throws SQLException, IOException {
        if (aggiorna) {
            return cmm.aggiornaSessione(sessione);
        }
        return cmm.salvaSessione(sessione);
    }

    public Sessione creaSessione() throws Exception {
        return cmm.creaSessione(spia);
    }

    public void shiftUser() throws SQLException {
        if (GuiController.getInstance() == null) {
            new GuiController();
        }
    }

    public List<Sessione> elencaSessioni() throws SQLException, IOException, ClassNotFoundException {
        return cmm.elencaSessioni(spia);
    }
    
    public boolean eliminaSessione(Sessione sessione) throws SQLException{
     return cmm.eliminaSessione(sessione);   
    }
}
