package sas.controller.studente;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import sas.controller.DBManager;
import sas.model.Proposta;
import sas.model.SistemaDiCifratura;
import sas.model.Studente;
import sas.model.UserInfo;
import sas.model.messaggio.Messaggio;
import sas.model.messaggio.MessaggioDestinatario;
import sas.model.messaggio.MessaggioMittente;

/**
 * Classe che si occupa solo della comunicazione
 */
public class CommunicationController {

    private static CommunicationController instance;

    /**
     * Metodo che invia il messaggio 
     * 
     * @param msg messaggio da inviare
     * @return  true se il messaggio è stato inviato
     * @throws java.sql.SQLException errore lato DB
     */
    public static boolean send(Messaggio msg) throws SQLException {
        return msg.send();
    }

    public static CommunicationController getInstance() {
        if (instance == null) {
            instance = new CommunicationController();
        }
        return instance;
    }

    /**
     * Metodo che restituisce i destinatari di una proposta accettata
     * 
     * @param usr studente che propone metodo di cifratura
     * @return la lista dei studenti destinatari della proposta accettata
     * @throws SQLException errore lato DB
     */
    public static List<UserInfo> getDestinatari(Studente usr) throws SQLException {
        int id = usr.getId();
        List<UserInfo> userInfo = new ArrayList<>();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM STUDENTE INNER JOIN PROPOSTA ON STUDENTE.ID = PROPOSTA.CREATORE WHERE STUDENTE.ID=" + id + " and STATO ='accettata' ");
        while (rs.next()) {
            userInfo.add(new UserInfo(DBManager.executeQuery("SELECT ID,NOME,COGNOME,USERNAME FROM STUDENTE WHERE ID=" + rs.getInt("PARTNER"))));
        }
        rs = DBManager.executeQuery("SELECT * FROM STUDENTE INNER JOIN PROPOSTA ON STUDENTE.ID = PROPOSTA.PARTNER WHERE STUDENTE.ID=" + id + " and STATO ='accettata' ");
        while (rs.next()) {
            userInfo.add(new UserInfo(DBManager.executeQuery("SELECT ID,NOME,COGNOME,USERNAME FROM STUDENTE WHERE ID=" + rs.getInt("CREATORE"))));
        }
        return userInfo;
    }

    /**
     * Metodo che crea una nuova proposta
     * 
     * @param usr utente che propone
     * @param partner utente a cui è stata effetuata la proposta
     * @param sdc sistema di cifratura
     * @return true se la proposta è stata salvata
     * @throws java.sql.SQLException errore lato DB
     */
    public static boolean inviaProposta(UserInfo usr, UserInfo partner, SistemaDiCifratura sdc) throws SQLException {
        Proposta proposta = new Proposta(usr, partner, sdc);
        return proposta.salva();
    }

    /**
     * Metodo che ritorna le proposte (accettate o rifiutate) ricevute dallo
     * studente e non ancora notificata
     * 
     * @param user studente che ha ricevuto delle proposte
     * @return lista di proposte non ancora notificate
     * @throws java.sql.SQLException errore lato DB
     */
    public static List<Proposta> getAccettazioneProposte(Studente user) throws SQLException, Exception {
        List<Proposta> proposta = new ArrayList<>();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM PROPOSTA WHERE CREATORE=" + user.getId() + " AND (STATO='accettata' OR STATO='rifiutata') AND NOTIFICATA=0");
        while (rs.next()) {
            proposta.add(new Proposta(rs));
        }
        return proposta;
    }

    /**
     * Metodo che ritorna la lista di proposte ricevute dallo studente e in
     * stato pending
     * 
     * @param usr studente che ha ricevuto delle proposte
     * @return lista di proposte pending
     * @throws java.sql.SQLException errore lato DB
     */
    public static List<Proposta> getProposte(Studente usr) throws SQLException, Exception {
        List<Proposta> proposte = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM PROPOSTA WHERE PARTNER=" + usr.getId() + " AND STATO='pendente'");
        while (rs.next()) {
            proposte.add(new Proposta(rs));
        }
        return proposte;
    }

    /**
     * Metodo aggiorna lo stato della proposta se la proposta è stata accettata
     * ed esiste una vecchia proposta tra le due parti , quest ultima assume lo
     * stato di scaduta e viene aggiunta la decisione corrente
     *
     * @param prop  proposta da aggiornare
     * @param decisione accetta,rifiuta
     * @return true se la proposta è stata aggiornata con successo
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public static boolean inviaDecisione(Proposta prop, String decisione) throws SQLException, Exception {
        if (decisione.equals("accetta")) {
            Proposta p = Proposta.caricaAttiva(prop.getProponente(), prop.getPartner());
            if (p != null) {
                p.setStato("scaduta");
                p.aggiorna();
            }
            prop.setStato("accettata");
        } else {
            prop.setStato("rifiutata");

        }
        return prop.aggiorna();
    }

    /**
     * Metodo usato per l apertura del messaggio tramite l id
     * 
     * @param id identificativo del messaggio
     * @return Messaggio
     * @throws SQLException errore lato DB
     */
    public static Messaggio apriMessaggio(int id) throws SQLException, Exception {
        Messaggio msg = Messaggio.load(id);
        msg.setLetto(true);
        msg.salva();
        return msg;
    }
    /**
     * Metodo che ritorna i messaggi ricevuti dal utente
     * @param user Studente
     * @return lista dei messaggi Ricevuti
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public static List<MessaggioDestinatario> getMessaggiRicevuti(Studente user) throws SQLException, Exception {
        return Messaggio.caricaRicevuti(user);
    }
    /**
     * Metodo che ritorna i messaggi inviati dal utente
     * @param user Studente
     * @return lista dei messaggi Inviati
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public static List<MessaggioMittente> getMessaggiInviati(Studente user) throws SQLException, Exception {
        return Messaggio.caricaInviati(user);
    }
    /**
     * Metodo che ritorna i messaggi bozza del utente
     * 
     * @param user Studente
     * @return lista dei messaggi Bozza
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public static List<MessaggioMittente> getMessaggiBozza(Studente user) throws SQLException, Exception {
        return Messaggio.caricaBozze(user);
    }

    
    /**
     * Metodo che ritorna le proposte del utente che non siano pendenti
     * @param user utente interessato (partner o creatore)
     * @return proposte
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public static List<Proposta> getCronologiaProposte(Studente user) throws SQLException, Exception {
        List<Proposta> proposte = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM PROPOSTA WHERE (PARTNER=" + user.getId() + " OR CREATORE="+user.getId()+") AND STATO!='pendente'");
        while (rs.next()) {
            proposte.add(new Proposta(rs));
        }
        return proposte;
    }
    /**
     * Metodo che salva il messaggio passato come parametro come bozza
     * 
     * @param msg messaggio da salvare
     * @return true se è stato salvato 
     * @throws SQLException errore lato DB
     */
    boolean salvaMessaggioBozza(Messaggio msg) throws SQLException {
        msg.setBozza(true);
        return msg.salva();
    }
    /**
     * Metodo che restituisce tutti gli utenti tranne l'utente corrente
     * 
     * @param user Studente 
     * @return lista di Utenti
     * @throws SQLException errore lato DB
     */
    public List<UserInfo> getUtenti(Studente user) throws SQLException {
        List<UserInfo> utenti = new ArrayList();
        CachedRowSet rs = DBManager.executeQuery("SELECT * FROM STUDENTE WHERE ID!=" + user.getId());
        utenti.add(new UserInfo(rs));
        return utenti;
    }
}
