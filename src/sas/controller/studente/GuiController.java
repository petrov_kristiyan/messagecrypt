package sas.controller.studente;

import java.sql.SQLException;
import java.util.List;
import sas.controller.UserFrame;
import static sas.controller.UserFrame.user;
import sas.controller.spia.GuiControllerSpia;
import sas.model.Mappatura;
import sas.model.Proposta;
import sas.model.SistemaDiCifratura;
import sas.model.UserInfo;
import sas.model.messaggio.Messaggio;
import sas.model.messaggio.MessaggioDestinatario;
import sas.model.messaggio.MessaggioMittente;
import sas.view.studente.StudenteForm;

/**
 * Classe utilizzata da tramite tra la Gui e il controller di comunicazione o il
 * Modello specifica per lo Studente
 */
public class GuiController extends UserFrame {

    private static StudenteForm studente;
    private static CommunicationController cmm;
    private static GuiController instance;

    public GuiController() throws SQLException {
        studente = new StudenteForm().getInstance(user, this);
        generateFrame(studente);
        cmm = new CommunicationController();
    }

    /**
     * Metodo che invia la decisione alla proposta scelta al
     * comunicationController
     *
     * @param proposta la proposta alla quale modifica lo stato
     * @param decisione lo stato scelto per la proposta (accetta,rifiuta)
     * @return true se è stata inviata
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public boolean comunicaDecisione(Proposta proposta, String decisione) throws SQLException, Exception {
        return CommunicationController.inviaDecisione(proposta, decisione);
    }

    /**
     * Metodo che ritorna l'istanza della Classe
     *
     * @return istanza del controller della GUI dello studente
     * @throws SQLException errore lato DB
     */
    public static GuiController getInstance() throws SQLException {
        if (instance == null) {
            instance = new GuiController();
        }
        return instance;
    }
    public void shiftUser() throws SQLException{
        if (GuiControllerSpia.getInstance() == null) {
            new GuiControllerSpia();
        }
        
    }
    /**
     * Metodo che richiede al communicationController le proposte accettate o
     * rifiutate ma non ancora notificate
     *
     * @return le Proposte del utente accettate o rifiutate ma non ancora
     * visualizzate/notificate
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<Proposta> vediNotificheAccettazioneProposte() throws SQLException, Exception {
        return CommunicationController.getAccettazioneProposte(user);

    }
    /**
     * Metodo che richiede al communicationController le proposte del utente che non siano pendenti
     * 
     * @return proposte
     * @throws Exception  errore generico
     */
    public List<Proposta> getCronologiaProposte() throws Exception{
        return CommunicationController.getCronologiaProposte(user);
    }

    /**
     * Metodo che richiede al communicationController le proposte con stato
     * pending
     *
     * @return le Proposte del utente con stato pendente
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<Proposta> vediProposteSistemaDiCifratura() throws SQLException, Exception {
        return CommunicationController.getProposte(user);

    }

    /**
     * Metodo che genera la Mappatura tramite il Sistema di cifratura
     *
     * @param key chiave scelta (per Cesare tutti i numeri interi, stringhe e/o
     * interi e/o stringhe alfanumeriche per Pseudocasuale , stringhe per Chiave
     * )
     * @param metodo metodo di crittografia (Cesare,Pseudocasuale,Chiave)
     * @return Mappatura generata dal sistema di cifratura
     * @throws Exception errore generico
     */
    public Mappatura generaMappatura(String key, String metodo) throws Exception {
        SistemaDiCifratura sdc = new SistemaDiCifratura(key, metodo);
        return sdc.getMappatura();
    }

    /**
     * Metodo utilizzato per provare il Sistema di cifratura, prima di salvarlo
     *
     * @param testo testo da cifrare tramite il sistema di cifratura
     * @param sdc sistema di cifratura da utilizzare
     * @return testocifrato
     */
    public String cifra(String testo, SistemaDiCifratura sdc) {
        return sdc.prova(testo);
    }

    /**
     * Metodo che richiede al communication controller di inviare il messaggio
     *
     * @param msg messaggio da inviare
     * @return true se il messaggio è stato spedito
     * @throws SQLException errore lato DB
     */
    public boolean spedisciMessaggio(Messaggio msg) throws SQLException {
        return CommunicationController.send(msg);
    }

    /**
     * Metodo utilizzato ottenere il messaggio tramite l'id
     *
     * @param id identificativo del messaggio da restituire
     * @return messaggioMittente
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public MessaggioMittente apriMessaggio(int id) throws SQLException, Exception {
        return Messaggio.load(id);
    }

    /**
     * Metodo che richiede al communication controller di aprire il Messaggio
     * ricevuto
     *
     * @param id identificativo del messaggio da aprire
     * @return messaggioDestinatario
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public MessaggioDestinatario apriMessaggioRicevuto(int id) throws SQLException, Exception {
        return CommunicationController.apriMessaggio(id);
    }

    /**
     * Metodo che richiede al communication controller di salvare un messaggio
     * come Bozza
     *
     * @param msg Messaggio da salvare
     * @return true se è stato salvato
     * @throws SQLException errore lato DB
     */
    public boolean salvaMessaggioBozza(Messaggio msg) throws SQLException {
        return cmm.salvaMessaggioBozza(msg);
    }

    /**
     * Metodo che richiede al CommunicationController i messaggi dell'utente
     * salvati come bozza
     *
     * @return lista di messaggioMittente
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<MessaggioMittente> elencaMessaggiBozza() throws SQLException, Exception {
        return CommunicationController.getMessaggiBozza(user);
    }

    /**
     * Metodo che richiede al CommunicationController i messaggi inviati
     * dall'utente
     *
     * @return lista di messaggioMittente
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<MessaggioMittente> elencaMessaggiInviati() throws SQLException, Exception {
        return CommunicationController.getMessaggiInviati(user);
    }

    /**
     * Metodo che richiede al CommunicationController i messaggi ricevuti
     * dall'utente
     *
     * @return lista di messaggioDestinatario
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<MessaggioDestinatario> elencaMessaggiRicevuti() throws SQLException, Exception {
        return CommunicationController.getMessaggiRicevuti(user);
    }
    /**
     * Metodo che elimina il messaggio
     * 
     * @param msg messaggio da cancellare
     * @throws SQLException errore lato DB
     */
    public void eliminaMessaggi(Messaggio msg) throws SQLException {
        msg.elimina();
    }
    /**
     * Metodo che elenca i Destinatari (solo quelli che hanno accettato una proposta)
     * 
     * @return lista degli utenti Destintari 
     * @throws SQLException errore lato DB
     */
    public List<UserInfo> elencaDestinatari() throws SQLException {
        return CommunicationController.getDestinatari(user);
    }
    /**
     * Metodo che cifra il messaggio
     * 
     * @param msg messaggio da cifrare
     * @throws SQLException errore lato DB
     */
    public void cifraMessaggio(Messaggio msg) throws SQLException, Exception {
        msg.cifra();
    }
    /**
     * Metodo che decifra il messaggio
     * 
     * @param msg messaggio da decifrare
     */
    public void decifraMessaggio(Messaggio msg) {
        msg.decifra();
    }
    /**
     * Metodo che restituisce i sistemi di cifratura creati dall'utente
     * 
     * @return lista dei Sistemi di cifratura
     * @throws SQLException errore lato DB
     * @throws Exception errore generico
     */
    public List<SistemaDiCifratura> elencaSistemaDiCifratura() throws SQLException, Exception {
        return SistemaDiCifratura.caricaSistemaDiCifratura(user);
    }
    /**
     * Metodo che elimina il Sistema di cifratura
     * 
     * @param sdc sistema di cifratura da eliminare
     * @return true se l'operazione ha avuto successo
     * @throws SQLException errore lato DB
     */
    public boolean eliminaSistemaDiCifratura(SistemaDiCifratura sdc) throws SQLException {
        return sdc.elimina();
    }
    /**
     * Metodo che salva il sistema di cifratura dell'utente
     * 
     * @param sdc sistema di cifratura da salvare nel DB
     * @return true se l'operazione ha avuto successo
     * @throws SQLException errore lato DB
     */
    public synchronized boolean salvaSistemaDiCifratura(SistemaDiCifratura sdc) throws SQLException {
        return sdc.salva(user);
    }
    /**
     * Metodo che fa il dispose del Frame dello studente
     */
    public void dispose() {
        studente.dispose();
    }
    /**
     * Metodo che restituisce tutti gli utenti tranne l'utente loggato
     * 
     * @return  lista degli utenti
     * @throws SQLException errore lato DB
     */
    public List<UserInfo> getUtenti() throws SQLException {
        return cmm.getUtenti(user);
    }
    /**
     * Metodo sincronizzato per inviare una proposta al partner
     * 
     * @param partner UserInfo a cui inviare la proposta
     * @param sdc sistema di cifratura proposto
     * @return true se l'operazione è avvenuta con successo
     * @throws SQLException errore lato DB
     */
    public synchronized boolean inviaProposta(UserInfo partner, SistemaDiCifratura sdc) throws SQLException {
        UserInfo proponente = new UserInfo(user.getId(), user.getNome(), user.getCognome(), user.getUsername());
        return CommunicationController.inviaProposta(proponente, partner, sdc);
    }
}
