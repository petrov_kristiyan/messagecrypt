package sas.controller;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import sas.model.Studente;
import sas.view.Login;

/**
 * Classe astratta utilizzata per generare i frame Studente e Spia
 */
public abstract class UserFrame {

    public static Studente user;

    /**
     * Metodo che genera i Frame
     * 
     * @param frame Jframe da generare con look del Sistema
     */
    public void generateFrame(final JFrame frame) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(frame, "Impossibile generare frame",  "Errore Critico",JOptionPane.ERROR_MESSAGE);
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            frame.setVisible(true);
        });
    }
}
