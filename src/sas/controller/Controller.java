package sas.controller;

import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;
import sas.controller.spia.GuiControllerSpia;
import sas.controller.studente.GuiController;
import sas.model.Studente;
import sas.view.Login;

/**
 * Classe controller principale implementata con singleton per garantire
 * l'esistenza di una sola istanza
 */
public class Controller extends UserFrame {

    private static Controller instance;
    private static sas.controller.studente.GuiController controllerStudente;
    private  sas.controller.spia.GuiControllerSpia controllerSpia;

    public Controller() {
        generateFrame(new Login());

    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    /**
     * Metodo per fare il login come studente
     * 
     * @param username username
     * @param pass password
     * @param tipo  true se è studente
     * @return true se l'utente esiste con quella password
     * @throws SQLException errore lato DB
     */
    public boolean login(String username, String pass, boolean tipo) throws SQLException {
        String query = "SELECT * FROM STUDENTE WHERE USERNAME ='" + username + "' AND PASSWORD = '" + pass + "'";
        CachedRowSet s = DBManager.executeQuery(query);
        user = new Studente(s);
        if (user.getNome() != null) {
            if (tipo) {
                controllerStudente = GuiController.getInstance();
            } else {
                controllerSpia = GuiControllerSpia.getInstance();
            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * Metodo utilizzato per chiudere il programma
     */
    public void logout()  {
        System.exit(0);
    }
}
