The project was done by using NetBeans so it is recommended to use it for this project.

You will need :
*Java 8
*JDBC
*MySQL
*JUnit

To get it running  you will need first to create the Database by using the sas.controller.DbSas.sql query
Access credentials:
user1 : mario     password:  mario
user2 : atta        password:  atta


If you get an error in the interface about JPanel1 you will need to set again that the Layout is AbsoluteLayout.