package test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import sas.model.Mappatura;
import sas.model.calcolatore.CalcolatoreCesare;
import sas.model.calcolatore.CalcolatorePseudo;

public class CalcolatoreTest {
    protected String chiaveP,chiaveC;
    protected CalcolatorePseudo pseudo;
    protected CalcolatoreCesare cesare;
    public CalcolatoreTest() {
    }
      
    @Before
    public void setUp() {
        chiaveP = "2a3";
        chiaveC ="2";
        pseudo = new CalcolatorePseudo();
        cesare = new CalcolatoreCesare();
    }
    
    /**
     * Test of calcola method, of class CalcolaCesare.
     */
    
    @Test
    public void testCalcolaPseudo(){
        System.out.println("calcolaPseudo");
        char[] mappa =   {'z','s','h','c','a','w','v','b','n','e','g','f','m','u','y','p','x','i','t','l','k','j','o','q','d','r'};
        char[] inversa = {'e','h','d','y','j','l','k','c','r','v','u','t','m','i','w','p','x','z','b','s','n','g','f','q','o','a'};
        Mappatura result = pseudo.calcola(chiaveP);
        assertArrayEquals(mappa, result.getMappatura());
        assertArrayEquals(inversa, result.getMappaturaInversa());
    }
    @Test
    public void testCalcolaCesare(){
        System.out.println("calcolaCesare");
        char[] mappa =   {'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','a','b'};
        char[] inversa = {'y','z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x'};
        try{
            Mappatura result = cesare.calcola(chiaveC);
            assertArrayEquals(mappa, result.getMappatura());
            assertArrayEquals(inversa, result.getMappaturaInversa());
        }
        catch(Exception e){
            Assert.fail();
        }
    }
    
}
