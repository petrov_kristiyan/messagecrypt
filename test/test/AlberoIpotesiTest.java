package test;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import sas.model.AlberoIpotesi;
import sas.model.Ipotesi;

public class AlberoIpotesiTest {
    protected AlberoIpotesi albero;
    protected Ipotesi ipotesi;
    protected List<Ipotesi> listaIpotesi;
    protected Ipotesi active;
    
    public AlberoIpotesiTest() {
    }
 
    @Before
    public void setUp() {
        LinkedHashMap<Character,List<Ipotesi>> lhm = new LinkedHashMap<>();
        listaIpotesi = new ArrayList();
        albero = new AlberoIpotesi(lhm);   
    }
    
    /**
     * Test aggiungi ipotesi
     */
    @Test
    public void testInsert() {
        System.out.println("aggiungi mossa");
        ipotesi = new Ipotesi(1,"test mossa",'a','c',true);
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi);
        assertEquals(ipotesi,albero.getAlbero().get(ipotesi.getChiave()).get(0));
    }

    @Test
    public void testUndo() {
        System.out.println("undo");
        ipotesi = new Ipotesi(2,"test undo",'a','d',false);
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi);
        assertEquals(ipotesi,albero.getAlbero().get(ipotesi.getChiave()).get(0));
    }
    /**
     * testa se prende correttamente l'ultima ipotesi aggiunta
     */
    @Test
    public void testMostraMosse(){
        System.out.println("mostra la mossa attiva");
        Ipotesi ipotesi1 = new Ipotesi(1,"test mossa",'a','e',true);
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi1);
        Ipotesi ipotesi2 = new Ipotesi(2,"test mossa",'a','e',false);
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi2);
        Ipotesi ipotesi3 = new Ipotesi(3,"test undo",'a','d',true);
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi3);
        Ipotesi ipotesi4 = new Ipotesi(4,"test mossa",'a','d',false); 
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi4);
        Ipotesi ipotesi5 = new Ipotesi(5,"test mossa",'a','f',true); 
        albero.aggiungiIpotesi('a', listaIpotesi, ipotesi5);
        listaIpotesi.forEach(e -> {      
        Ipotesi active = listaIpotesi.get(albero.lastIndexOf(listaIpotesi, e));
         if(active.isOperazione()){
             this.active =active;
         }
        });
        assertEquals(ipotesi5,active); 
    }
}
